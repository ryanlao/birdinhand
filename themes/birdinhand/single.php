<?php get_header(); ?>

<!-- blocks/events-packages-intro -->
<?php get_template_part( 'blocks/popups'); ?>
            	
				<?php while ( have_posts() ) : the_post(); ?>
       
					<?php get_template_part( 'blocks/content', get_post_type() ); ?>
					<div class="container">
					<?php if ( function_exists( 'echo_ald_crp' ) ) echo_ald_crp(); ?>
					<div class="clearfix"></div>
					<h3 style="margin-top:30px;">Related Events:</h3>
					<?php echo do_shortcode('[tribe_events_list category="village-events" limit="3"]'); ?>
					</div>
					<?php get_template_part( 'blocks/pager-single', get_post_type() ); ?>
				<?php endwhile; ?>
              
		</main>

<?php get_footer(); ?>
