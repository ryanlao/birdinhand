if(jQuery('body').hasClass('page-id-25')) { 

var places = [
  {
    title: "Bird-in-Hand Stage",
    description: "<p>2760 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 790-4069<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0380988, lng: -76.174466}
  }
];

} else if(jQuery('body').hasClass('page-id-641') || jQuery('body').hasClass('page-id-192')) { 

var places = [
  {
    title: "Amish Country Motel",
    description: "<p>3013 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 538-2535 &bull; (717) 768-8396<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0390411, lng: -76.1561844}
  }
];

} else if(jQuery('body').hasClass('page-id-651')) { 

var places = [
  {
    title: "Bird-in-Hand Family Inn",
    description: "<p>2740 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 537-2535 &bull; (717) 768-8271<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0379914, lng: -76.1757686}
  }
];

} else if(jQuery('body').hasClass('page-id-662')) { 

var places = [
  {
    title: "Bird-in-Hand Village Inn &amp; Suites",
    description: "<p>2695 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 914-2473 &bull; (717) 768-1535<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0387462, lng: -76.1799845}
  }
];

} else if(jQuery('body').hasClass('page-id-644')) { 

var places = [
  {
    title: "Country Acres Campground",
    description: "<p>20 Leven Road<br/>Gordonville, PA 17529<br/>(866) 675-4745 &bull; (717) 687-8014<br/><a href='mailto:CountryAcres@Bird-in-Hand.com'>CountryAcres@Bird-in-Hand.com</a>",
    location: {lat: 40.0148101, lng: -76.1444627}
  }
];

} else if(jQuery('body').hasClass('page-id-656')) { 

var places = [
  {
    title: "Travelers Rest Motel",
    description: "<p>3701 Old Philadelphia Pike<br/>Intercourse, PA 17534<br/>(800) 626-2021 &bull; (717) 768-8731<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0391803, lng: -76.101657}
  }
];

} else if(jQuery('body').hasClass('page-id-956')) { 

var places = [
  {
    title: "Bird-in-Hand Family Restaurant &amp; Smorgasbord",
    description: "<p>2760 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(717) 768-1500 &bull; Call-ahead seating (717) 768-1550<br/><a href='mailto:SmuckerFamily@Bird-in-Hand.com'>SmuckerFamily@Bird-in-Hand.com</a>",
    location: {lat: 40.0380988, lng: -76.174466}
  }
];

} 
else if(jQuery('body').hasClass('page-id-21')) { 

var places = [
  {
    title: "Bird-in-Hand Family Restaurant &amp; Smorgasbord",
    description: "<p>2760 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(717) 768-1500 &bull; Call-ahead seating (717) 768-1550<br/><a href='mailto:SmuckerFamily@Bird-in-Hand.com'>SmuckerFamily@Bird-in-Hand.com</a>",
  location: {lat: 40.0380988, lng: -76.174466}
  },{
    title: "Bird-in-Hand Bakery &amp; Cafe",
    description: "<p>2715 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 524-3429 &bull; (717) 768-1501<br/><a href='mailto:SmuckerFamily@Bird-in-Hand.com'>SmuckerFamily@Bird-in-Hand.com</a>",
    location: {lat: 40.0387714, lng: -76.1780885}
  }

];

} 

else if(jQuery('body').hasClass('page-id-23')) { 

var places = [
  {
    title: "Bird-in-Hand Bakery &amp; Cafe",
    description: "<p>2715 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 524-3429 &bull; (717) 768-1501<br/><a href='mailto:SmuckerFamily@Bird-in-Hand.com'>SmuckerFamily@Bird-in-Hand.com</a>",
    location: {lat: 40.0387714, lng: -76.1780885}
  }
];

} else if(jQuery('body').hasClass('page-id-19')) { 

var places = [
  {
    title: "Amish Country Motel",
    description: "<p>3013 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 538-2535 &bull; (717) 768-8396<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0390411, lng: -76.1561844}
  },{
    title: "Bird-in-Hand Family Inn",
    description: "<p>2740 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 537-2535 &bull; (717) 768-8271<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0379914, lng: -76.1757686}
  },{
    title: "Bird-in-Hand Village Inn &amp; Suites",
    description: "<p>2695 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 914-2473 &bull; (717) 768-1535<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0387462, lng: -76.1799845}
  },{
    title: "Country Acres Campground",
    description: "<p>20 Leven Road<br/>Gordonville, PA 17529<br/>(866) 675-4745 &bull; (717) 687-8014<br/><a href='mailto:CountryAcres@Bird-in-Hand.com'>CountryAcres@Bird-in-Hand.com</a>",
    location: {lat: 40.0148101, lng: -76.1444627}
  },{
    title: "Travelers Rest Motel",
    description: "<p>3701 Old Philadelphia Pike<br/>Intercourse, PA 17534<br/>(800) 626-2021 &bull; (717) 768-8731<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0391803, lng: -76.101657}
  }
];

} else { 

var places = [
  {
    title: "Amish Country Motel",
    description: "<p>3013 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 538-2535 &bull; (717) 768-8396<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0390411, lng: -76.1561844}
  },{
    title: "Bird-in-Hand Bakery &amp; Cafe",
    description: "<p>2715 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 524-3429 &bull; (717) 768-1501<br/><a href='mailto:SmuckerFamily@Bird-in-Hand.com'>SmuckerFamily@Bird-in-Hand.com</a>",
    location: {lat: 40.0387714, lng: -76.1780885}
  },{
    title: "Bird-in-Hand Family Inn",
    description: "<p>2740 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 537-2535 &bull; (717) 768-8271<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0379914, lng: -76.1757686}
  },{
    title: "Bird-in-Hand Family Restaurant &amp; Smorgasbord",
    description: "<p>2760 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(717) 768-1500 &bull; Call-ahead seating (717) 768-1550<br/><a href='mailto:SmuckerFamily@Bird-in-Hand.com'>SmuckerFamily@Bird-in-Hand.com</a>",
    location: {lat: 40.0382988, lng: -76.174766}
  },{
    title: "Bird-in-Hand Stage",
    description: "<p>2760 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 790-4069<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0379888, lng: -76.174066}
  },{
    title: "Bird-in-Hand Village Inn &amp; Suites",
    description: "<p>2695 Old Philadelphia Pike<br/>Bird-in-Hand, PA 17505<br/>(800) 914-2473 &bull; (717) 768-1535<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0387462, lng: -76.1799845}
  },{
    title: "Country Acres Campground",
    description: "<p>20 Leven Road<br/>Gordonville, PA 17529<br/>(866) 675-4745 &bull; (717) 687-8014<br/><a href='mailto:CountryAcres@Bird-in-Hand.com'>CountryAcres@Bird-in-Hand.com</a>",
    location: {lat: 40.0148101, lng: -76.1444627}
  },{
    title: "Travelers Rest Motel",
    description: "<p>3701 Old Philadelphia Pike<br/>Intercourse, PA 17534<br/>(800) 626-2021 &bull; (717) 768-8731<br/><a href='mailto:CentralReservations@Bird-in-Hand.com'>CentralReservations@Bird-in-Hand.com</a>",
    location: {lat: 40.0391803, lng: -76.101657}
  }
];


}