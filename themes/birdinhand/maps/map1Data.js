var places = [
  {
    title: "Brecknock Orchards",
    description: "<p>390 Orchard Road, Mohnton</p><i>Blackberries, black raspberries, red raspberries, strawberries</i>",
    location: {lat: 40.226065, lng: -76.037682},
    image: "http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/farmstand-white.png"
  },{
    title: "Jake &amp; Lizzie Ebersole",
    description: "<p>New Holland</p><i>Grape tomatoes</i>",
    location: {lat: 40.1017643, lng: -76.084227},
    image: "http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/farmstand-white.png"
  },{
    title: "Eshbenshade Turkeys",
    description: "<p>Paradise</p><i>Turkey</i>",
    location: {lat: 40.0050807, lng: -76.1348913},
    image: "http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/farmstand-white.png"
  },{
    title: "Elam Fisher",
    description: "<p>Kirkwood</p><i>Blackberries</i>",
    location: {lat: 39.8562317, lng: -76.0856502},
    image: "http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/farmstand-white.png"
  },{
    title: "Sylvan Fisher",
    description: "<p>Ronks</p><i>Sweet corn, tomatoes</i>",
    location: {lat: 40.0262259, lng: -76.1773208}
  },{
    title: "Flinchbaugh's Orchard &amp; Farm Market",
    description: "<p>110 Ducktown Road, York</p><i>Peaches</i>",
    location: {lat: 40.0083369, lng: -76.5807245}
  },{
    title: "Groff Meats",
    description: "<p>Elizabethtown</p><i>Pork, dried beef, liver, beef tips</i>",
    location: {lat: 40.1554505, lng: -76.616899}
  },{
    title: "Aiden &amp; Miriam Hoover",
    description: "<p>1107 Sheep Hill Road, New Holland</p><i>Peaches, watermelon</i>",
    location: {lat: 40.0935149, lng: -76.043546}
  },{
    title: "Ephraim &amp; Sarah Huyard",
    description: "<p>2965-B Church Road, Bird-in-Hand</p><i>Hanging baskets, mums</i>",
    location: {lat: 40.0460117, lng: -76.1599816}
  },{
    title: "Levi &amp; Rebecca Huyard",
    description: "<p>New Holland</p><i>Broccoli, cabbage, green beans, green peppers, red and green leaf lettuce, new potatoes, red potatoes, romaine lettuce, sugar peas, yellow wax beans, zucchini</i>",
    location: {lat: 40.1017643, lng: -76.086227}
  },{
    title: "Kauffman's Fruit Farm",
    description: "<p>3097 Old Philadelphia Pike, Bird-in-Hand</p><i>Apples, peaches</i>",
    location: {lat: 40.0392593, lng: -76.1446269}
  },{
    title: "Oasis at Bird-in-Hand",
    description: "<p>60 North Ronks Road, Suite 1, Ronks</p><i>Milk, butter, eggs, cheese</i>",
    location: {lat: 40.0206147, lng: -76.1676878}
  },{
    title: "Sauder's",
    description: "<p>Lititz</p><i>Eggs, cheese</i>",
    location: {lat: 40.1527344, lng: -76.321098}
  },{
    title: "John &amp; Myrna Smucker",
    description: "<p>Bird-in-Hand</p><i>Angus beef, pumpkins</i>",
    location: {lat: 40.0387139, lng: -76.1843658}
  },{
    title: "Smucker Farms",
    description: "<p>Bird-in-Hand</p><i>Eggs</i>",
    location: {lat: 40.0387139, lng: -76.1823658}
  },{
    title: "Irwin &amp; Florence Stauffer",
    description: "<p>661 Ranck Road, New Holland</p><i>Cantaloupe, watermelon</i>",
    location: {lat: 40.094884, lng: -76.047538}
  },{
    title: "Jonas &amp; Annie Stolzfus",
    description: "<p>2605 Stumptown Road, Bird-in-Hand</p><i>Cucumbers, red beets</i>",
    location: {lat: 40.05606, lng: -76.195643}
  },{
    title: "Stoltzfus Meats",
    description: "<p>Intercourse</p><i>Scrapple, sausage</i>",
    location: {lat: 40.0368395, lng: -76.1275501}
  },{
    title: "Weaver Turkeys",
    description: "<p>Leola</p><i>Turkey</i>",
    location: {lat: 40.0878717, lng: -76.1974918}
  }
];