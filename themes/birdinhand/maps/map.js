// Initiallize Map //
var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(40.15, -76.3),
    keyboardShortcuts: true,
    scrollwheel: true,
    disableDefaultUI: true,
    mapTypeControl: true,
    mapTypeControlOptions: { position: google.maps.ControlPosition.TOP_RIGHT },
    panControl: true,
    panControlOptions: { position: google.maps.ControlPosition.RIGHT_TOP },
    zoomControl: true,
    zoomControlOptions: { position: google.maps.ControlPosition.RIGHT_CENTER },
    styles: [{"featureType":"landscape","stylers":[{"hue":"#FFAD00"},{"saturation":50.2},{"lightness":-34.8},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFAD00"},{"saturation":-19.8},{"lightness":-1.8},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FFAD00"},{"saturation":72.4},{"lightness":-32.6},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FFAD00"},{"saturation":74.4},{"lightness":-18},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#00FFA6"},{"saturation":-63.2},{"lightness":38},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#FFC300"},{"saturation":54.2},{"lightness":-14.4},{"gamma":1}]}]
};

var map = new google.maps.Map(document.getElementById('map_canvas'),
    mapOptions
);



// Set Feature Styles //
var featureStyleDefault = {
    strokeColor : '#996600',
    strokeWeight : 1 ,
    fillColor : '#996600' ,
    fillOpacity : 0.3 ,
    clickable : true ,
    draggable : false ,
    editable : false
};
var featureStyleOptimal = {
    strokeColor : '#7BC618',
    strokeWeight : 1 ,
    fillColor : '#7BC618' ,
    fillOpacity : 0.3 ,
    clickable : true ,
    draggable : false ,
    editable : false
};
var featureStyleBelowOptimal = {
    strokeColor : '#9966DD',
    strokeWeight : 1 ,
    fillColor : '#9966DD' ,
    fillOpacity : 0.3 ,
    clickable : true ,
    draggable : false ,
    editable : false
};

map.data.setStyle(function(feature){
    //console.log(feature);
    var color = feature.getProperty('color');
    if (color != null) {
        map.data.overrideStyle(feature, {
            strokeColor: color,
            fillColor: color
        });
    }
    var weight = feature.getProperty('weight');
    if (weight != null) {
        map.data.overrideStyle(feature, {
            strokeWeight: weight,
            fillOpacity: 0
        });
    }
    var concentration = feature.getProperty('fluorideConcentration');
    if (concentration === 'optimal') {
        return featureStyleOptimal;
    }
    if (concentration === 'below optimal') {
        return featureStyleBelowOptimal;
    }
    return featureStyleDefault;
});

// Add Features to Map from Previously Loaded Data 'markets' //
var feature;
for (var i in places) {
    feature = new google.maps.Data.Feature({
        geometry: new google.maps.Data.Point( places[i].location ),
        id: null,
        properties: places[i],
        icon: 'http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/farmstand-white.png'
    });
    map.data.add(feature);
}


// Open Infowindow when Features are Clicked //
var infowindow;
map.data.event.addListener(marker, 'mouseout', function(){
          infowindow.close();
       });
map.data.addListener('click', function(event) {
    console.log(event);
    if (infowindow != null) {
        infowindow.close();
    }
    if (event.feature) {
        var title = '<h4>' + event.feature.getProperty('title') + '</h4>';
        var description = event.feature.getProperty('description');
        if (description) {
            infowindow = new google.maps.InfoWindow({
                content: title + description,
                position: event.latLng
            });
            infowindow.open(map);
        }
    }
});

google.maps.event.addListener(map, "click", function(event) {
    infowindow.close();
});

// Geocoding //
var geocoder = new google.maps.Geocoder();
var marker;
function goToAddress (event) {
    event.preventDefault();
    var address = jQuery('#address-lookup-text').val();
    geocoder.geocode({address: address, bounds: countyBounds}, function(results, status){
        if (status == google.maps.GeocoderStatus.OK) {
            //console.log(results);
    
            // Zoom to top result. //
            map.fitBounds(results[0].geometry.viewport);

            // Clear previous marker. //
            if (marker) {
                try { marker.setMap(null) }
                catch(err){}; // Just ignore errors. //
            }

            // Create new marker at top result. //
            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                title: results[0].formatted_address,
                animation: google.maps.Animation.DROP
            });

        } else {
            alert("Sorry, I couldn't find that place.\nPlease check the address and try again.");
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function popLegalInfo (event) {
    event.preventDefault();
    var notes = jQuery('#legal-notes').html();
    alert(notes);
}

// Attach Geocoding Actions to Form //
jQuery(document).ready(function(){
    jQuery('#address-lookup-form').submit(goToAddress);
    jQuery('#legal-link').click(popLegalInfo);
});