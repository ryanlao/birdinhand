		
<main id="main" role="main">

		<div class="fixed-block side-menu">
		
			<ul class="list-holder">
            
                                    
            	                
                            	<li>
                                    
                                    <a data-popup="popup-lodging" class="open" href="../lodging/">
                                       <i class="fa fa-bed"></i>
                                                                                    <span>Places to Stay</span><i class="icon-arrow-right"></i></a>
                                                                               
                                    <div class="popup">
                                        <div class="head-block">
                                        <a class="close" href="#">X</a>
                                                                                        
                                        </div>
                                        <div class="scroll-wrap">
                                            
                                            <div class="block">
                                                                                                                                                    <div class="img-area" style="background-image: url(http://birdinhand.wpengine.com/wp-content/uploads/2015/12/AmishFarmland_Donovan-1680x896-1111x513.jpg);">
                                                        <strong class="title">Amish Farmlands in Lancaster County, PA</strong>
                                                    </div>
                                                                                           
                                                <div class="popup-intro">
                                                                                                                        
                                                                
                                                            
                                                        
                                                    
                                                                                                                
                                                        																		                                                                            <div class="col-md-12">
                                                                                <h1><strong>We Welcome You to Stay With Us</strong></h1>
A perfect day in Lancaster County - and the perfect lodging place at the end of it. A spotless room with clean, crisp sheets. When your head hits the pillow, you drift off knowing you’ll be rested and ready for tomorrow’s adventure.

If you’re looking for places to stay in Lancaster County, we invite you to experience our deep-rooted tradition of hospitality. The Smucker family has welcomed visitors to the historic village of Bird-in-Hand in the heart of the region’s Amish farmlands for generations. Today, our properties put you in the perfect location to enjoy all of the shopping, sightseeing, dining and entertainment that the region has to offer.

&nbsp;
<h2><strong>A Stay Like No Other</strong></h2>
When it comes to lodging in Amish Country, you have many choices. Whether you are looking for hotels in Lancaster County for an overnight visit, an intimate weekend get-away for two or a memorable family vacation with activities for the kids, one of our properties will meet your needs. From hayrides and free ice cream socials with the Smucker family, to conversations with our neighbors about Amish history and beliefs, our Guests enjoy exclusive events they’ll treasure forever. Every stay at any of our properties includes a complimentary two-hour tour of scenic back roads and Amish farmlands. So reserve your place with us today - and remember to ask about our seasonal specials and VIP program. Find out for yourself why so many of our Guests become friends who return year after year.                                                                            </div>
                                                                                                                                             
                                                                    
                                                                
                                                            
                                            
                                                                              <div class="col-md-12 pos-static">
                                    	
                                        
                                        
                                        	
                                    </div>
                                                
                                                </div>
                                                                                                
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                            
                            
            	                
                            	<li>
                                    
                                    <a data-popup="popup-dining" class="open" href="../dining/">
                                       <i class="fa fa-cutlery"></i>
                                                                                    <span>Places to Dine</span><i class="icon-arrow-right"></i></a>
                                                                               
                                    <div class="popup">
                                        <div class="head-block">
                                        <a class="close" href="#">X</a>
                                                                                        
                                        </div>
                                        <div class="scroll-wrap">
                                            
                                            <div class="block">
                                                                                                                                                    <div class="img-area" style="background-image: url(http://birdinhand.wpengine.com/wp-content/uploads/2015/12/FarmFreshSpread2-1680x896-1111x513.jpg);">
                                                        <strong class="title">Farm Fresh Produce</strong>
                                                    </div>
                                                                                           
                                                <div class="popup-intro">
                                                                                                                        
                                                                
                                                            
                                                        
                                                    
                                                                                                                
                                                        																		                                                                            <div class="col-md-12">
                                                                                <h1>Enjoy Our Bounty</h1>
The first bite brings back mouth-watering memories - fresh-picked produce... hearty, homemade soups, artisan breads… juicy Angus beef raised on the Smucker farm…The Smucker family knows good food. We have been growing and preparing our own for eight generations, and we have been feeding visitors to our part of Lancaster County since 1970, when we opened our first Bird-in-Hand restaurant. Today, guests return time after time to our <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">Bird-in-Hand Family Restaurant &amp; Smorgasbord</a> and our <a href="http://birdinhand.wpengine.com/bakery/">Bird-in-Hand Bakery &amp; Cafe</a> . Our offerings range from Pennsylvania Dutch recipes like chicken corn soup, ham balls with pineapple sauce and chicken pot pie to handcrafted sandwiches on homemade bread to specialty cakes and artisan ice cream. We make most of what we serve on our premises from local produce, meats, dairy and other farm-fresh ingredients - and you can taste the difference.

&nbsp;                                                                            </div>
                                                                                                                                             
                                                                    
                                                                
                                                            
                                            
                                                                              <div class="col-md-12 pos-static">
                                    	
                                        
                                        
                                        	
                                    </div>
                                                
                                                </div>
                                                                                                
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                            
                                       	<li>
                                    
                                    <a data-popup="popup-dining" class="open" href="../dining/">
                                       <i class="fa fa-picture-o"></i>
                                                                                    <span>Things to Do</span><i class="icon-arrow-right"></i></a>
                                                                               
                                    <div class="popup">
                                        <div class="head-block">
                                        <a class="close" href="#">X</a>
                                                                                        
                                        </div>
                                        <div class="scroll-wrap">
                                            
                                            <div class="block">
                                                                                                                                                    <div class="img-area" style="background-image: url(http://birdinhand.wpengine.com/wp-content/uploads/2015/12/FarmFreshSpread2-1680x896-1111x513.jpg);">
                                                        <strong class="title">Farm Fresh Produce</strong>
                                                    </div>
                                                                                           
                                                <div class="popup-intro">
                                                                                                                        
                                                                
                                                            
                                                        
                                                    
                                                                                                                
                                                        																		                                                                            <div class="col-md-12">

<div class="row">
<h2>Amish Farmland Tour</h2>
<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/FarmlandTour-400x280.jpg" rel="attachment wp-att-1695"><img class="alignleft wp-image-1727 size-medium" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/FarmlandTour-400x280-300x210.jpg" alt="" width="300" height="210" /></a>
A two-hour complimentary tour of the Amish farmlands is provided exclusively for Guests. Reservations required. Daily tours Monday-Saturday.
</div>  
  
 
 
 
 
 <div class="row">
<h2>Bird-in-Hand Stage</h2>
<a href="http://birdinhand.wpengine.com/wp-content/uploads/2015/12/ConfessionArtwork-428x274.jpg" rel="attachment wp-att-1285"><img class="alignright wp-image-1285 size-medium" src="http://birdinhand.wpengine.com/wp-content/uploads/2015/12/ConfessionArtwork-428x274-300x192.jpg" alt="ConfessionArtwork-428x274" width="300" height="192" /></a>

Our Bird-in-Hand Stage offers live entertainment including musicals and magic shows. It’s an intimate theatrical experience that keeps Guests coming back.  <a href="http://birdinhand.wpengine.com/stage/">Click here </a> to view our 2016 season. Lodging Guests receive a discount to our shows.
 </div>
 
 
 
  <div class="row">
<h2>Gathering Place</h2>
<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/GatheringPlace1.jpg" rel="attachment wp-att-1742"><img class="alignleft size-thumbnail wp-image-1742" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/GatheringPlace1-150x150.jpg" alt="GatheringPlace1" width="300" height="192" /></a>A great place for family and friends to gather around our outdoor fire pit. Located between the Front and Pool Buildings at the Bird-in-Hand Family Inn, this space is available for family and group rental during the spring, summer and fall. S’mores packages available.
 
 </div>
 
 
 
 
  
 
  <div class="row">
<h2>Guest Activities at the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/">Bird-in-Hand Family Inn</a></h2>
<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/PettingZoo.jpg" rel="attachment wp-att-1734"><img class="alignright wp-image-1734 size-full" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/PettingZoo.jpg" alt="PettingZoo" width="300" height="192" /></a>A variety of amenities and activities are available exclusively to Guests of Bird-in-Hand lodging properties at no additional charge including:
<ul>
 	<li>Fitness Center - work out in our fitness room with an elliptical machine and treadmills</li>
 	<li>Game Room - enjoy playing a variety of arcade games</li>
 	<li>Petting Zoo - our petting zoo is located behind Waters Edge Mini Golf and is a favorite among the kids</li>
 	<li>Playground - the outdoor playground has swings, a slide and other equipment.</li>
 	<li>Pond and walking trail - a beautiful pond with a quarter mile walking trail is located behind the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">Bird-in-Hand Family Restaurant</a>.</li>
 	<li>Pools - large outdoor pool with walk-in entrance, two indoor pools and a large hot tub</li>
 	<li>Tennis and basketball courts - lighted courts behind the Bird-in-Hand Family Inn.</li>
</ul>
 </div>
 
 
 
 <div class="row">
<h2>Hot Air Balloon Rides</h2>
<h2> <a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BalloonLaunch.jpg" rel="attachment wp-att-1736"><img class="alignleft wp-image-1736 size-full" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BalloonLaunch.jpg" alt="BalloonLaunch" width="400" height="280" /></a></h2>
Hot air balloons launch across from the Family Inn by the <a href="http://www.ushotairballoon.com" target="_blank">US Hot Air Balloon Team</a>. Fees apply.
</div>
 
 
  
  
  <div class="row">
<h2>Miniature Golf</h2>
<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/WatersEdgeMiniGolf.jpg" rel="attachment wp-att-1737"><img class="alignright wp-image-1737 size-full" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/WatersEdgeMiniGolf.jpg" alt="WatersEdgeMiniGolf" width="400" height="280" /></a>

<a href="http://www.watersedgegolf.net" target="_blank">Waters Edge Mini Golf</a> is located directly behind the Bird-in-Hand Family Restaurant & Smorgasbord. Fees apply to play.

 </div>
 
 
 
 
 
 
 
  <div class="row">
<h2>Shopping</h2>
<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/shopping1.jpg" rel="attachment wp-att-1738"><img class="alignleft wp-image-1738 size-full" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/shopping1.jpg" alt="shopping1" width="400" height="280" /></a>Enjoy shopping in the picturesque <a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/2015BIHTownMapGuideFINAL.pdf" target="_blank">village of Bird-in-Hand</a> or the nearby town of Intercourse. Our properties are also minutes from the Bird-in-Hand Farmer&#039;s Market, Kitchen Kettle Village, Rockvale Outlets and Tanger Outlets.
 
 
 
 
 
 
 
 

For a complete list of activities in the Lancaster area, visit <a href="http://www.discoverlancaster.com/index.asp" target="_blank">Discover Lancaster</a>.
 </div>
&nbsp;                                                                            </div>
                                                                                                                                             
                                                                    
                                                                
                                                            
                                            
                                                                              <div class="col-md-12 pos-static">
                                    	
                                        
                                        
                                        	
                                    </div>
                                                
                                                </div>
                                                                                                
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </li>
            	                
                       
                                            
                            
            	                
                            	<li>
                                    
                                    <a data-popup="popup-groups" class="open" href="#">
                                       <i class="fa fa-user"></i>
                                                                                    <span>Groups</span><i class="icon-arrow-right"></i></a>
                                                                               
                                    <div class="popup">
                                        <div class="head-block">
                                        <a class="close" href="#">X</a>
                                                                                            <h1>Groups</h1>
                                                                                        
                                        </div>
                                        <div class="scroll-wrap">
                                            
                                            <div class="block">
                                                                                                                                                    <div class="img-area" style="background-image: url(http://birdinhand.wpengine.com/wp-content/uploads/2016/01/farmlandphoto-1111x513.png);">
                                                        <strong class="title"></strong>
                                                    </div>
                                                                                           
                                                <div class="popup-intro">
                                                                                                                        
                                                                
                                                            
                                                        
                                                    
                                                                                                                
                                                        																		                                                                            <div class="col-md-12">
                                                                                <h1><strong>We Welcome Friends, Families, Bus Tours and Other Groups</strong></h1>
They say, “The more, the merrier,” and we find it to be true. Whether you’re planning a family reunion or want to add a Pennsylvania Dutch smorgasbord or an Amish Country theatre experience to your bus tour itinerary, you are in good hands with the Bird-in-Hand Family of Companies. From small meetings to banquets, we are happy to accommodate your group. We offer high-quality lodging and dining options and individualized service as well as special dining experiences and other packages unique to Amish Country. Our Sales team is happy to help you plan menus, accommodations, entertainment and things to do in Lancaster County - whatever you need to make your event a success.

For group hotel rates, meeting space rentals, information on our restaurant or group tickets for our Bird-in-Hand Stage, call us at (800) 627-1910 or <a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>.

&nbsp;                                                                            </div>
                                                                                                                                             
                                                                    
                                                                
                                                            
                                            
                                                                              <div class="col-md-12 pos-static">
                                                                        	<div class="nav-tabs-container"><div class="scrollingHotSpotLeft" style="display: none;"></div><div class="scrollingHotSpotRight" style="display: none;"></div><div class="scrollWrapper"><div class="scrollableArea" style="width: 951px;">
                                            <ul class="nav nav-tabs" role="tablist"><li role="presentation" class=" active"><a href="#Lodging" aria-controls="Lodging" role="tab" data-toggle="tab">Lodging</a></li><li role="presentation" class=" inactive"><a href="#Dining" aria-controls="Dining" role="tab" data-toggle="tab">Dining</a></li><li role="presentation" class=" inactive"><a href="#Stage" aria-controls="Stage" role="tab" data-toggle="tab">Stage</a></li><li role="presentation" class=" inactive"><a href="#BanquetsandMeetings" aria-controls="BanquetsandMeetings" role="tab" data-toggle="tab">Banquets and Meetings</a></li><li role="presentation" class=" inactive"><a href="#SpecialExperiences" aria-controls="SpecialExperiences" role="tab" data-toggle="tab">Special Experiences</a></li><li role="presentation" class=" inactive"><a href="#Weddings" aria-controls="Weddings" role="tab" data-toggle="tab">Weddings</a></li>                                            </ul>
                                        </div></div></div>
                                        	
                                        
                                        
                                                                                        
                                            <div class="tab-content">
                                                                                                                                                        <div role="tabpanel" class="tab-pane active" id="Lodging"><h2>Bring Your Group to Stay With Us</h2>
<h2></h2>
<p>At the end of a busy, fun-filled day, treat your group members to the warmth of our hospitality – and a comfortable, clean place to rest their heads. At the Bird-in-Hand Family of Properties, your group will enjoy a welcoming atmosphere, non-smoking rooms and a range of conveniences at our inns and motels, including free in-room Wi-Fi, cable television, hair dryers and refrigerators for beverages and snacks. As Guests of any of our inns or motels, your group can also enjoy the many recreational amenities at our resort-style <a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/">Bird-in-Hand Family Inn</a>, including heated indoor and outdoor pools, large hot tub, game room, tennis and basketball courts, walking trails and acres of lawn. From a special welcome on arrival to smiling, helpful service throughout your stay, we will do what it takes to make sure your group has a memorable time.</p>
<p>Groups enjoy these benefits with our compliments:</p>
<ul>
<li>Express check-in and luggage handling</li>
<li>Complimentary room for every 20 paid</li>
<li>Free hot breakfast buffet in our <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">restaurant</a></li>
<li>Gift for driver and group leader</li>
</ul>
<p>To help your members get to know our unique region, we offer a complimentary, <a href="http://birdinhand.wpengine.com/get-off-the-beaten-path-and-see-our-back-roads">two-hour tour of local farmlands</a>, escorted by our trained and experienced guides, and a complimentary screening of an award-winning film about the Amish.</p>
<p>For your convenience, we offer a free on-site dump station and service area for tour buses using our property, and drivers and group leaders are welcome to use our fax machine, photocopier and other business services.</p>
<p>Give your group a great experience. Ask us about customized group packages including lodging, meals, attractions and tours. For group hotel rates at the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/">Bird-in-Hand Family Inn</a> or <a href="http://birdinhand.wpengine.com/bird-in-hand-village-inn-suites/">Bird-in-Hand Village Inn &amp; Suites</a>, call us at (800) 627-1910 or&nbsp;<a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>. For group hotel rates at <a href="http://birdinhand.wpengine.com/travelers-rest-motel/">Travelers Rest Motel</a>, please call (800) 626-2021 and for <a href="http://birdinhand.wpengine.com/country-acres-campground/">Country Acres Campground</a>, please call (866) 675-4745.</p>
                                                                                                            </div>
                                                    
                                                                                                                                                        <div role="tabpanel" class="tab-pane inactive" id="Dining"><h2>Satisfy Everyone’s Appetite<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/BIHRestaurant-400x267.jpg"><img class="alignnone size-full wp-image-1394" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/BIHRestaurant-400x267.jpg" alt="BIHRestaurant-400x267" width="400" height="267" srcset="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/BIHRestaurant-400x267-300x200.jpg 300w, http://birdinhand.wpengine.com/wp-content/uploads/2016/01/BIHRestaurant-400x267.jpg 400w" sizes="(max-width: 400px) 100vw, 400px"></a></h2>
<p>How do you handle a hungry crowd? Offer them good, fresh, homemade food – and plenty of it. At the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">Bird-in-Hand Family Restaurant &amp; Smorgasbord</a>, we can accommodate groups as large as 250. When it comes to our cooking, we use only the freshest ingredients, sourcing as much as possible from local farms. Your group can enjoy traditional Pennsylvania Dutch food, including many of Grandma Smucker’s original recipes, as well as a variety of other delicious dishes – so there’s something for everyone on our menu. If your group includes youngsters, they’ll be delighted with the kid-friendly favorites on our unique Noah’s Ark children’s buffet. And everyone will want to save room for dessert, including our famous shoofly pie and other mouthwatering treats from our <a href="http://birdinhand.wpengine.com/bakery/bakery-cafe-menu/">Bird-in-Hand Bakery &amp; Cafe</a>.</p>
<p>When your group dines with us, we offer:</p>
<ul>
<li>Group dining for up to 175</li>
<li>Banquet seating for up to 250</li>
<li>Private dining for 20 or more</li>
<li>Several dining-style options, including smorgasbord, soup-and-salad bar, menu dining and family-style meals</li>
<li>Complimentary meal for motorcoach driver and escort<br>
• Full service from 6 a.m. until 8 p.m. Monday – Saturday</li>
<li>Private, ground-level covered entrance for motorcoach guests</li>
<li>Accessible entry to both banquet and main-floor dining areas</li>
<li>Unique dining experiences, including an Amish Christmas Celebration, Amish Wedding Feast and a special package with the Landis Valley Village &amp; Farm Museum [LINK TO Special Experiences tab in the Groups subpage]</li>
<li>Special value packages with Sight &amp; Sound Theatres, showing Samson March 12 – December 31, 2016</li>
<li>Student discounts</li>
</ul>
<p>Treat your guests to Bird-in-Hand hospitality and the home cooking that put us on the map. To learn more about the packages our <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">Bird-in-Hand Family Restaurant </a>can put together for your group, call (800) 627-1910 or <a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>.</p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/HorsDoeuvresBreaks2016-1.pdf" target="”_new”">Hors D’oeuvres &amp; Breaks Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetBreakfast2016-1.pdf" target="”_new”">Breakfast Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetLunch2016-1.pdf" target="”_new”">Banquet Lunch Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetBuffetFS2016-1.pdf" target="”_new”">Buffet &amp; Family Style Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetServedBanquets2016-1.pdf" target="”_new”">Served Banquets Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/StudentAthleticMenu2016-1.pdf" target="”_new”">Student &amp; Athletic Group Menu</a></p>
                                                                                                            </div>
                                                    
                                                                                                                                                        <div role="tabpanel" class="tab-pane inactive" id="Stage"><h2>Sit Back and Enjoy the Show<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/Confession-400x267.jpg"><img class="alignnone size-full wp-image-1380" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/Confession-400x267.jpg" alt="Confession-400x267" width="400" height="267" srcset="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/Confession-400x267-300x200.jpg 300w, http://birdinhand.wpengine.com/wp-content/uploads/2016/01/Confession-400x267.jpg 400w" sizes="(max-width: 400px) 100vw, 400px"></a></h2>
<p>We welcome your group to the <a href="http://birdinhand.wpengine.com/stage/">Bird-in-Hand Stage</a>, where you can enjoy Broadway-style musicals set in Amish Country and master illusionists. Currently located on the banquet level of the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">Bird-in-Hand Family Restaurant &amp; Smorgasbord</a>, our stage features tiered seating for up to 175, and accessible floor seating is available. The stage offers its own ground-level entrance and is accessible by elevator from the main dining room upstairs.</p>
<p>The following musicals are live on our stage in 2016. New for this year is our first Christmas show.</p>
<ul>
<li>The Confession: May 5 – July 14 [LINK TO Confession landing page]<br>
An Amish Love Story</li>
<li>Josiah for President: July 22 – November 5 [LINK TO Josiah landing page]&nbsp;Can Plain Truth Triumph Over Politics?</li>
</ul>
<ul>
<li>Our Christmas Dinner: November 11 – December 31 [LINK TO Christmas landing page]<br>
It’s the Most, Wonderful Meal of the Year!</li>
</ul>
<p>New for 2016 – experience a theatrical event for the entire family.</p>
<ul>
<li>The Magic &amp; Wonder Show: April 21 – October 22 [LINK TO Magic &amp; Wonder landing page]<br>
A Breathtaking Experience for All Ages</li>
</ul>
<p>We offer discounted show tickets for groups of 20 or more, with complimentary tickets for the driver and tour escort. For your convenience and value, <a href="http://birdinhand.wpengine.com/stage/">Bird-in-Hand Stage</a> tickets are also available as part of a discounted lunch, dinner and/or lodging package.</p>
<p>To reserve your group tickets, call us at (800) 627-1910 or <a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>.</p>
                                                                                                            </div>
                                                    
                                                                                                                                                        <div role="tabpanel" class="tab-pane inactive" id="BanquetsandMeetings"><h2>Fresh Food and Extra-Special Events<a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/VIMeetingSpace_Table-400x300.jpg"><img class="alignnone size-full wp-image-1395" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/VIMeetingSpace_Table-400x300.jpg" alt="VIMeetingSpace_Table-400x300" width="400" height="300" srcset="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/VIMeetingSpace_Table-400x300-300x225.jpg 300w, http://birdinhand.wpengine.com/wp-content/uploads/2016/01/VIMeetingSpace_Table-400x300.jpg 400w" sizes="(max-width: 400px) 100vw, 400px"></a></h2>
<p>Whether you’re planning a club luncheon or an awards dinner, a sales meeting, motorcoach tour or a family reunion, you want every detail to be right. So do we. We work hard to help make your banquet or special event a success – with farm-fresh food, our tested recipes and warm, friendly service that will make every one of your guests feel welcome.</p>
<p>If you are organizing a large event, we can serve groups of up to 175 at our <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/">Bird-in-Hand Family Restaurant &amp; Smorgasbord</a>, and we offer served, plated meals, or buffet or family-style service. We offer banquet seating for up to 250, and we also offer comfortable meeting rooms for families and other small groups at the resort-style <a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/">Bird-in-Hand Family Inn</a>. In addition, the Desmond House Gathering Place in our <a href="http://birdinhand.wpengine.com/bird-in-hand-village-inn-suites/">Village Inn &amp; Suites</a> accommodates up to 35 guests for small meetings, retreats and social gatherings.</p>
<p>We can provide display tables, a podium and microphone, easel and skirted head table at no extra cost, and our facilities all offer free Wi-Fi. Audio-visual presentation equipment is also available for rent.</p>
<p>For reservations, call (800) 627-1910 or <a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>.</p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/HorsDoeuvresBreaks2016-1.pdf" target="”_new”">Hors D’oeuvres &amp; Breaks Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetBreakfast2016-1.pdf" target="”_new”">Breakfast Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetLunch2016-1.pdf" target="”_new”">Banquet Lunch Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetBuffetFS2016-1.pdf" target="”_new”">Buffet &amp; Family Style Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/BanquetServedBanquets2016-1.pdf" target="”_new”">Served Banquets Menu</a></p>
<p><a class="button" href="http://birdinhand.wpengine.com/wp-content/uploads/2016/02/StudentAthleticMenu2016-1.pdf" target="”_new”">Student &amp; Athletic Group Menu</a></p>
                                                                                                            </div>
                                                    
                                                                                                                                                        <div role="tabpanel" class="tab-pane inactive" id="SpecialExperiences"><h2>Authentic Amish Country Experiences</h2>
<p><a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/LandisValley-400x267.jpg"><img class="alignnone size-full wp-image-1382" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/LandisValley-400x267.jpg" alt="LandisValley-400x267" width="400" height="267" srcset="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/LandisValley-400x267-300x200.jpg 300w, http://birdinhand.wpengine.com/wp-content/uploads/2016/01/LandisValley-400x267.jpg 400w" sizes="(max-width: 400px) 100vw, 400px"></a></p>
<p>Steeped in history and tradition, rural Lancaster County is a fascinating destination with a unique culture. We invite you and your group to join us behind the scenes and experience the Amish culture first-hand – to be a part of an Amish celebration or to step back in time to a bygone era and experience the lifestyle, cooking, farming and crafts of the Amish, Mennonites and Pennsylvania Dutch. Our special experience packages will keep your guests talking and give them memories they’ll want to share for a long time to come.</p>
<p>Our special group experiences include:</p>
<p><strong>Experience Living History</strong><br>
Our <strong>Landis Valley Village &amp; Farm Museum Package</strong> includes a horse-drawn wagon ride around the Victorian village and the 18th-century farm and homestead, plus a sampling of sticky buns, <em>schnitz</em> pie and cider as they tour the original 1856 <a href="http://www.landisvalleymuseum.org" target="_blank">Landis Valley House Hotel &amp; Tavern</a>. After a morning at Landis Valley, your group will enjoy authentic Pennsylvania Dutch dining at the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/" target="_blank">Bird-in-Hand Family Restaurant &amp; Smorgasbord</a>, featuring a wide selection of home-cooked, farm-fresh dishes and desserts; our homemade soups, fresh salads and home-baked breads; and tempting treats from our <a href="http://birdinhand.wpengine.com/bakery/" target="_blank">Bird-in-Hand Bakery &amp; Cafe</a>. To round out the experience, we can include tickets to a Broadway-style musical or magic show on our <a href="http://birdinhand.wpengine.com/stage/" target="_blank">Bird-in-Hand Stage</a>.</p>
<p><strong>Celebrate Love</strong><br>
Our <strong>Amish Wedding Feast</strong> involves your group in a two-hour celebration in a private banquet room. Our New Order Amish friend will welcome your guests and help them select a “bride” and “groom” along with two traditional Amish “side sitters” before everyone partakes of a traditional Amish wedding turkey roast, ham loaf, chow chow and other Pennsylvania Dutch favorite side dishes. Everyone goes home with a gift to remember this special occasion.</p>
<p><strong>Join Us for the Holidays</strong><br>
At an <strong>Amish Christmas Celebration</strong> in our private banquet room, your group will learn about “Second Christmas,” family game times, children’s programs and other local Christmas traditions. Guests will also enjoy stamping with one of our New Order Amish friends, and savor a sumptuous Christmas feast featuring baked ham with pineapple sauce, an Amish roast turkey with bread filling, mashed potatoes with gravy and other favorites.</p>
<p>These special experiential dining packages are available with or without lodging. To arrange a memorable experience for your group, call us at (800) 627-1910 or <a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>.</p>
                                                                                                            </div>
                                                    
                                                                                                                                                        <div role="tabpanel" class="tab-pane inactive" id="Weddings"><h2>For a Day You’ll Always Remember</h2>
<p><a href="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/RedRosesWeddingCake-277x267.jpg"><img class="alignnone wp-image-1410 size-full" src="http://birdinhand.wpengine.com/wp-content/uploads/2016/01/RedRosesWeddingCake-277x267.jpg" alt="" width="227" height="267"></a></p>
<p>The romance… The ring… And now, preparations for the wedding. From our rehearsal dinner and reception facilities to our fresh delicious food to our scenic sites for wedding photos, we are here to help you create memories that you and your guests will treasure forever.</p>
<p>We offer reception facilities for up to 250 guests and can customize a package to suit your needs. If your reception is held at the <a href="http://birdinhand.wpengine.com/bird-in-hand-family-restaurant-smorgasbord/" target="_blank">Bird-in-Hand Family Restaurant</a> or the <a href="http://birdinhand.wpengine.com/bird-in-hand-village-inn-suites/" target="_blank">Bird-in-Hand Village Inn &amp; Suites</a>, receive a 10% discount on your custom, made from scratch wedding cake from <a href="http://birdinhand.wpengine.com/bakery/bakery-cafe-menu/" target="_blank">Bird-in-Hand Bakery &amp; Cafe</a> in your choice of cake, icing and fillings; choose from a variety of traditional and contemporary styles and shapes with flowers, cake lace and other enhancements [LINK TO Restaurant, Village Inn, wedding cake gallery and Bakery subpages]</p>
<p>Enjoy an overnight stay at the romantic <a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/" target="_blank">Bird-in-Hand Village Inn &amp; Suites</a> the evening of the reception and a delicious breakfast smorgasbord the next morning. And, because we understand that the details make the difference, we offer an array of options that include buffet or family style meal service; special room rates for your overnight guests; customized décor, including flowers, candles, centerpieces, ice sculptures and a candelabra for the head table; and use of our scenic outdoor gazebo.</p>
<p>For smaller weddings or rehearsal dinners, consider the Desmond House Gathering Place in our <a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/" target="_blank">Village Inn &amp; Suites</a>. This charming Victorian space accommodates up to 24 dinner guests in an intimate setting.</p>
<p>To discuss a customized package for your wedding, call us at (800) 627-1910 or <a href="mailto:groupsales@Bird-in-Hand.com?subject=Group inquiry from web">email us</a>.</p>
                                                                                                            </div>
                                                    
                                                                                            </div>
                                        	
                                    </div>
                                                
                                                </div>
                                                                                                
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                            
                            
            	                
                            	<li>
                                    
                                    <a data-popup="popup-mnd" class="open" href="#">
                                       <i class="fa fa-map-marker"></i>
                                                                                    <span>Map &amp; Directions</span><i class="icon-arrow-right"></i></a>
                                                                               
                                    <div class="popup">
                                        <div class="head-block">
                                        <a class="close" href="#">X</a>
                                                                                            <h1>Map &amp; Directions</h1>
                                                                                        
                                        </div>
                                        <div class="scroll-wrap">
                                            
                                            <div class="block">
                                                                                           
                                                <div class="popup-intro">
                                                                                                                        
                                                                
                                                            
                                                        
                                                    
                                                                                                                    <div id="map_canvas" style="overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 710px; top: -124px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 454px; top: -124px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 454px; top: 132px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 710px; top: 132px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 198px; top: -124px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 198px; top: 132px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 966px; top: -124px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 966px; top: 132px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -58px; top: 132px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -58px; top: -124px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 1222px; top: -124px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 1222px; top: 132px;"></div></div></div></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 710px; top: -124px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 454px; top: -124px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 454px; top: 132px;"><canvas draggable="false" height="384" width="384" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 710px; top: 132px;"><canvas draggable="false" height="384" width="384" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 198px; top: -124px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 198px; top: 132px;"><canvas draggable="false" height="384" width="384" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 966px; top: -124px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 966px; top: 132px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -58px; top: 132px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -58px; top: -124px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 1222px; top: -124px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 1222px; top: 132px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="transform: translateZ(0px); position: absolute; left: 710px; top: -124px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2364!3i3099!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=113737" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 454px; top: -124px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2363!3i3099!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=55882" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 454px; top: 132px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2363!3i3100!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=36336" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 710px; top: 132px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2364!3i3100!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=94191" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 198px; top: -124px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2362!3i3099!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=129098" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 198px; top: 132px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2362!3i3100!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=109552" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 966px; top: -124px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2365!3i3099!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=40521" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 966px; top: 132px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2365!3i3100!4i256!2m3!1e0!2sm!3i338005131!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=66476" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -58px; top: 132px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2361!3i3100!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=51697" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -58px; top: -124px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2361!3i3099!4i256!2m3!1e0!2sm!3i338005119!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=71243" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 1222px; top: -124px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2366!3i3099!4i256!2m3!1e0!2sm!3i338004976!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=45075" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 1222px; top: 132px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i2366!3i3100!4i256!2m3!1e0!2sm!3i338005131!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkFEMDB8cC5zOjUwLjJ8cC5sOi0zNC44fHAuZzoxLHMudDo0OXxwLmg6I0ZGQUQwMHxwLnM6LTE5Ljh8cC5sOi0xLjh8cC5nOjEscy50OjUwfHAuaDojRkZBRDAwfHAuczo3Mi40fHAubDotMzIuNnxwLmc6MSxzLnQ6NTF8cC5oOiNGRkFEMDB8cC5zOjc0LjR8cC5sOi0xOHxwLmc6MSxzLnQ6NnxwLmg6IzAwRkZBNnxwLnM6LTYzLjJ8cC5sOjM4fHAuZzoxLHMudDoyfHAuaDojRkZDMzAwfHAuczo1NC4yfHAubDotMTQuNHxwLmc6MQ!4e0!5m1!5f2&amp;token=124331" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 462px; top: 60px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 227px; bottom: 0px; width: 163px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 131px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; margin-top: 10px; overflow: hidden; display: none; margin-right: 14px; position: absolute; top: 55px; right: 0px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/sv5.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@40.0420411,-76.1301844,13z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint" draggable="false" controlwidth="28" controlheight="55" style="margin: 10px; -webkit-user-select: none; position: absolute; right: 28px; top: 113px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 0px;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; width: 28px; height: 55px; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; top: 0px; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; right: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show street map" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 28px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Map</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 35px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show satellite imagery" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-left-width: 0px; min-width: 58px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satellite</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 35px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=40.042041,-76.130184&amp;z=13&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/google4_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div></div></div>
                                                                                                                
                                                                                                                    <div class="col-md-12" style="position:relative;top:300px;">
                                                                
                                                                    
                                                       
                                                                        <p>The Bird-in-Hand family of properties are located in the historic village of Bird-in-Hand in the heart of Lancaster County’s Amish farmlands.&nbsp; The Smucker family invites you to get to know our village, to taste the farm-fresh goodness of its bounty and to experience the tradition of hospitality and good food that we have passed down from generation to generation.</p>
<p>For a complete list of properties, please <a href="http://birdinhand.wpengine.com/contact-our-sales-team/contact-us/">click here</a>.</p>
                                                            
                                                            </div>
                                                                     
                                                                    
                                                                
                                                            
                                            
                                                                              <div class="col-md-12 pos-static">
                                    	
                                        
                                        
                                        	
                                    </div>
                                                
                                                </div>
                                                                                                
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                            
                                				
			</ul>
			
		</div>