<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<?php 
		$top_img_bg  = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	?>
	<div class="top-img-bg title" style="<?php if($top_img_bg) echo "background-image:url('$top_img_bg');" ?>">
			<?php if ( is_single() ) :
				the_title( '<h1>', '</h1>' );
			else :
				the_title( '<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif; ?>		
	</div>
    <?php if(is_single()): ?><div class="top-img-bg-push" style="margin-bottom:0px;"></div><?php endif; ?>
    <?php get_template_part( 'blocks/reservations'); ?>
	<div class="content" style="padding-top:30px;">
	<div class="container">
		<?php 
			if(is_single){
				// the_post_thumbnail( 'full' ); 
			}else{
				the_post_thumbnail( 'full' ); 
			}
		?>
		<?php if ( is_single() ) :
			the_content();
		else:
			theme_the_excerpt();
		endif; ?>
	</div>
	</div>
	<?php wp_link_pages(); ?>

</div>
