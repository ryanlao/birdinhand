<div class="post">
	<div class="head">
		<h1><?php _e( 'Not Found', 'birdinhand' ); ?></h1>
	</div>
	<div class="content">
		<p><?php _e( 'Sorry, but you are looking for something that isn\'t here.', 'birdinhand' ); ?></p>
		<?php get_search_form(); ?>
	</div>
</div>
