		<div class="row">
		


<?php
/* Get the Page Slug to Use as a Body Class, this will only return a value on pages! */
$class = '';
/* is it a page */

	global $post;
        /* Get an array of Ancestors and Parents if they exist */
	$parents = get_post_ancestors( $post->ID );
        /* Get the top Level page->ID count base 1, array base 0 so -1 */ 
	$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
	/* Get the parent and set the $class with the page slug (post_name) */
        $parent = get_post( $id );
	$class = $parent->post_name;
    if ($class == 'dining'): ?> 
    <div class="col-md-12 reservations-manual" style="background:#fff;">
           <h3>Restaurant call-ahead seating - (717) 768-1550 - Please call within one hour of arrival.
</h3>
           <?php elseif ($class == 'stage'):?>
        <div class="col-md-12 reservations-manual" style="background:#fff;">
            <div class="col-md-9 "><h3>Call (800) 790-4069 to purchase tickets or book online: </h3> </div><div class="col-md-3  no-border"><a id="btnBook" href="http://tix5.centerstageticketing.com/sites/birdinhand61/">Buy Tickets Online.</a></div>
            <?php elseif ($class == 'bakery'):?>
            <div class="col-md-12 reservations-manual" style="background:#fff;">
            <div class="col-md-9"><h3> Our mission is to make delicious, handcrafted food onsite from the freshest ingredients.</h3> </div><div class="col-md-3 no-border"><a id="btnBook" href="#popup-cafemenu">View Our Menu</a></div>
               <?php elseif ($class == 'country-acres-campground'):?>
                <div class="col-md-12 reservations-manual" style="background:#fff;">
             <div class="col-md-9"><h3>Please fill out our reservation request and we will call you to confirm:</h3></div><div class="col-md-3  no-border"><a id="btnBook" href="http://birdinhand.wpengine.com/country-acres-campground/country-acres-campground-reservation-request/">Reservation Form</a></div>

<?php else: ?>      
 <div class="col-md-12 reservations" style="background:#fff;">          


<a class="res-mobile hidden-sm hidden-md hidden-lg visible-xs ">Reservations &gt;</a>
<div class="res-wrap">
<div class="res-width">
<p class="res-title hidden-xs">RESERVATIONS</p>
<div class="divide-bar"></div>
   <div class="PropertySelection col-md-2 col-sm-12 col-xs-12 ">
        <i class="fa fa-map-marker"></i>
        <select id="ddProperties" class="w_dropdown" required="" onchange="SetProperty()">
            <option value="" selected="selected">Choose a location</option>
            <option value="kbB%2bMAwqMAc6lllsVmqZ5uJSqLY%2fSvC%2bTo7CCkvWeWM%3d">Amish Country Motel</option>
            <option value="pMQR3%2bvnPvRIMq9uTewhsfeoCdiFqhn2Rt%2bJwQKLhWU%3d">Bird-in-Hand Family Inn</option>
            <option value="JqYxrlKqvYJoqyF3wjcvmaRf38Jrp13zXLfIEAC6LM8%3d">Bird-in-Hand Village Inn &amp; Suites</option>
            <!-- <option value="z%2bTw7k5pSZwscG41%2bWN6kcLvRta%2faTrqO9JwpAhacFw%3d">Country Acres Campground</option> -->
            <option value="vj107SJVxDBh8JnlWLHV2j%2bIrrPmLP3lyi%2bZtGym5%2bM%3d">Travelers Rest Motel</option>
        </select>
    </div>
    <div class="divide-bar"></div>
        <div class="divArrivalDate col-md-3 col-sm-2 col-xs-2">
            <div class="arrive pull-left">
            <input id="w_txtArrivalDate" readonly="true" class="w_textbox txtDates" onchange="SetArrivalDate()" type="text" />
            </div>
                        <span id="lblNights" class="w_label" style="display:none;">Nights</span>
            <input id="w_txtNights" style="display:none;" class="w_textbox txtNights" onchange="NightsChanged()" onkeydown="return CheckNumbers(event)" type="text" />
            <span class="pull-left" style="margin-top: 3px; margin-right: 10px;">to</span>
            <div class="depart pull-left">
            <input id="w_txtDepartureDate" readonly="true" class="w_textbox txtDates" onchange="SetDepartureDate()" type="text" />
            </div>
        </div>

    
<div class="divide-bar"></div>
        <div class="divAdults col-md-3 col-sm-2 col-xs-2">
            <i class="fa fa-user"></i>
            <select id="ddAdults" class="w_dropdown" onchange="SetAdults()">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="11">12</option>
            </select>
            <span id="lblAdults" class="w_label">Adults</span><br class="reservation">
            <?php if ($class !='bird-in-hand-village-inn-suites'): ?>
            <select id="ddChildren" class="w_dropdown" onchange="SetChildren()">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
        
                 <?php else: ?>
                 <p style="font-size:10px; margin:0;">Please Call For Children</p>
                 <style type="text/css">
                     #lblChildren { display:none; }
                     .divAdults { margin-top:7px !important; }
                 </style>
                 <?php endif;?>
            <span id="lblChildren" class="w_label">Children</span>

       </div>
       <div class="divide-bar"></div>
    <div class="promo-code">
        <i class="fa fa-pencil"></i>
        <input type="text" placeholder="Promo code">
    </div>
    </div>
   <div class="no-border">
        <input id="btnBook" type="button" value="Book Now" class="btnBook" onclick="BookNow()" />
	</div>	
        <input id="w_hfArrivalDate" type="hidden" />


<?php endif; ?>	
            	</div>
 </div>
</div>
 <script type="text/javascript">


     jQuery(document).ready(function(){
        jQuery('input#w_txtArrivalDate').val('Arrival Date');
        jQuery('input#w_txtDepartureDate').val('Departure Date');
        jQuery('.reservations .arrive img.ui-datepicker-trigger').detach().prependTo('.reservations .arrive')
        jQuery('.reservations .depart img.ui-datepicker-trigger').detach().prependTo('.reservations .depart')

        jQuery('#ui-datepicker-div').click(function(){
            jQuery('.reservations .depart img.ui-datepicker-trigger').detach().prependTo('.reservations .depart')
        })

        jQuery('input#w_txtArrivalDate').click(function(){
            jQuery('.arrive .ui-datepicker-trigger').trigger("click");
            jQuery('.reservations .depart img.ui-datepicker-trigger').detach().prependTo('.reservations .depart')
        })
        jQuery('input#w_txtDepartureDate').click(function(){
            jQuery('.depart .ui-datepicker-trigger').trigger("click");
            jQuery('.reservations .depart img.ui-datepicker-trigger').detach().prependTo('.reservations .depart')
        })


     })

jQuery(window).load(function(){
        var resTotal = jQuery('body').width();
        var resWidth = jQuery('.res-width').width();
        var newWidth = resTotal - resWidth - 26;

        if(jQuery(window).width < 1300) { 
            var newWidth = resTotal - resWidth - 151;
        }

        jQuery('.reservations .no-border').css("width", newWidth);

        if(jQuery('.res-mobile').is(':visible')){
            jQuery('.res-mobile').click(function(){
                jQuery('.res-wrap').toggle();
            })
        }

});



 </script>