<div class="row">
	<div class="container">
				<div class="col-wrap">	
	<div class="popups-trigger" >
		
			 
            <div id="iv2ib">
            
            	<?php if(have_rows('inner_V2_image_blocks_section')):?>
                
                	<?php $iv2ib_section_cnt = 0; ?>
                
                	<?php while (have_rows('inner_V2_image_blocks_section')): the_row(); $iv2ib_section_cnt++; ?>
                    
                    	<div class="inner-v2-image-blocks-section<?php if($iv2ib_section_cnt == 1) echo " f "; ?>">
                        
                        	<?php 
							
							$iv2ib_section_title = '';
							$iv2ib_section_title = get_sub_field('inner_V2_section_title');
							
							if($iv2ib_section_title): ?>
                        
                        	<div class="iv2ib-section-title">
                            	<h2><?php echo $iv2ib_section_title; ?></h2>
                            </div>
                            
                            <?php endif; //if iv2ib_section_title ?>
                            
                            <div class="iv2ib-image-blocks">
                            
                            	<?php if(have_rows('inner_V2_image_block')):?>
                                
                                	<ul class="list-holder">
                
                					<?php while (have_rows('inner_V2_image_block')): the_row(); ?>
                                    
                                    	 <li class="col room-list">
                                        
                                            <a class="open" href="#">
                    
                                                <div class="col-area">
                                                    <?php if($image=get_sub_field("image")):?>
                                                        <img src="<?php echo $image["sizes"]["thumbnail_400x280"];?>" alt="<?php echo $image["alt"];?>">
                                                    <?php endif;?>
                                                    <div class="text-block">
                                                        <?php if($title=get_sub_field("title")):?>
                                                            <h2><?php echo $title;?></h2>
                                                        <?php endif;?>
                                                
                                                    </div>
                                                </div>
                                                
                                                
                                                </a>
                                    
                                            <div class="popup">
                                                <div class="head-block">
                                                <a class="close" href="#">X</a>
                                            
                                            
                                                        <h1><?php the_sub_field("title"); ?></h1>
                            
                            
                                
                                                
                                                    
                                                </div>
                                                <div class="scroll-wrap">
                                                    <div class="block jcf-scrollable">
                                            
                                                                    <section class="room-carousel-box v2 ">
                                                                        
                                                                        <div class="carousel">
                                                                            <div class="mask">
                                                                                <div class="slideset">
                                                                                    <?php 
                    
                                                                                    $images = get_sub_field('photo_gallery');
                    
                                                                                    if( $images ): ?>
                                                                                    
                                                                                            <?php foreach( $images as $image ): ?>
                                                                                                <div class="slide">
                                                                                                    <div class="slide-holder rooms">
                                                                                                
                                                                                                         <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                                                                    
                                                                                                
                                                                                                
                                                                                                    </div>
                                                                                                </div>
                                                                                            <?php endforeach; ?>
                                                                                    
                                                                                    <?php endif; ?>
                                                                                                    
                                                                                </div>
                                                                            </div>
                                                                             <a class="btn-prev" href="#"><i class="icon-arrow-left"></i></a>
                                                                            <a class="btn-next" href="#"><i class="icon-arrow-right"></i></a>
                                                                        </div>
                                                                           
                                                                
                                                                
                                                                    </section>
                                                                
                                                    
                                                
                                                                <article class="post" >
                                                                
                                                                    <?php the_sub_field("text"); ?>
                                                                
                                                                
                                                                </article>
                                                
                                                        
                                                    
                                                        
                                                            
                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    
                                    <?php endwhile; //while have rows inner_V2_image_block  ?>
                                    
                                    </ul>
                                    
								<?php endif; //endif have rows inner_V2_image_block ?>
                            
                            </div>
                        
                        </div>
                    
                    <?php endwhile; //while have rows inner_V2_image_blocks_section  ?>
                    
                    
                
                <?php endif; //if have rows inner_V2_image_blocks_section ?>
			</div>		
			
		</div>

	<?php wp_reset_postdata();?>
		
				</div>
			</div>
</div>

	
 