<div class="row">
    <div class="col-md-8 col-md-offset-2" style="margin-bottom:50px;">
    	<div class="row">
        <?php if( have_rows('blocks') ):?>
			<?php while( have_rows('blocks') ): the_row();?>
                <?php if(get_sub_field('section_title')):?>
        <?php 
											    $section_name_string = get_sub_field("section_title");
											    $section_name_link = preg_replace("/[^a-zA-Z0-9]/", "", $section_name_string);
								
										    ?>
               <div class="col-md-3 new-width">  
                    <a class="button" href="#<?php echo $section_name_link; ?>"><?php the_sub_field("section_title"); ?></a>
                </div>
            <?php endif; ?>
            <?php endwhile; ?>       
        <?php endif; ?>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
				<div class="col-wrap">	
	<div class="popups-trigger">
		
			<ul class="list-holder">
				<?php if( have_rows('blocks') ):?>
										<?php while( have_rows('blocks') ): the_row();?>
                <?php if(get_sub_field('section_title')):?>
                 <?php 
											    $section_name_string = get_sub_field("section_title");
											    $section_name_link = preg_replace("/[^a-zA-Z0-9]/", "", $section_name_string);
								
										    ?>
					<li class="col-md-12"><h2><?php the_sub_field("section_title"); ?></h2>
                        <a name="<?php echo  $section_name_link; ?>" style=" display: block;
    position: relative;
    top: -150px;
    visibility: hidden;"></li>
                <li class="col room-list">
					
						<a class="open" href="#">

							<div class="col-area">
								<?php if($image=get_sub_field("image")):?>
									<img src="<?php echo $image["sizes"]["thumbnail_400x280"];?>" alt="<?php echo $image["alt"];?>">
								<?php endif;?>
								<div class="text-block">
									<?php if($title=get_sub_field("title")):?>
										<h2><?php echo $title;?></h2>
									<?php endif;?>
							
								</div>
							</div>
							
							
							</a>
				
						<div class="popup">
							<div class="head-block">
							<a class="close" href="#">X</a>
						
						
									<h1><?php the_sub_field("title"); ?></h1>
		
		
			
							
								
							</div>
							<div class="scroll-wrap">
								<div class="block jcf-scrollable">
						
												<section class="room-carousel-box v1">
													
													<div class="carousel">
														<div class="mask">
															<div class="slideset">
																<?php 

																$images = get_sub_field('photo_gallery');

																if( $images ): ?>
																
																		<?php foreach( $images as $image ): ?>
																			<div class="slide">
																				<div class="slide-holder rooms">
																			
																					 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																				
																			
																			
																				</div>
																			</div>
																		<?php endforeach; ?>
																
																<?php endif; ?>
																				
															</div>
														</div>
                                                        <a class="btn-prev" href="#"><i class="icon-arrow-left"></i></a>
														<a class="btn-next" href="#"><i class="icon-arrow-right"></i></a>
													</div>
														
											
											
												</section>
											
								
							
											<article class="post" >
											
											<a href="<?php the_field("room_booking_link"); ?>" class="button pull-left">Reserve A Room Now >></a> 
										
												<?php the_sub_field("text"); ?>
											
											
											</article>
							
									
								
									
										
							
								</div>
							</div>
						</div>
					</li>
                <?php else: ?>            					
					<li class="col room-list">
					
						<a class="open" href="#">

							<div class="col-area">
								<?php if($image=get_sub_field("image")):?>
									<img src="<?php echo $image["sizes"]["thumbnail_400x280"];?>" alt="<?php echo $image["alt"];?>">
								<?php endif;?>
								<div class="text-block">
									<?php if($title=get_sub_field("title")):?>
										<h2><?php echo $title;?></h2>
									<?php endif;?>
							
								</div>
							</div>
							
							
							</a>
				
						<div class="popup">
							<div class="head-block">
							<a class="close" href="#">X</a>
						
						
									<h1><?php the_sub_field("title"); ?></h1>
		
		
			
							
								
							</div>
							<div class="scroll-wrap">
								<div class="block jcf-scrollable">
						
												<section class="room-carousel-box v1a">
													
													<div class="carousel">
														<div class="mask">
															<div class="slideset">
																<?php 

																$images = get_sub_field('photo_gallery');

																if( $images ): ?>
																
																		<?php foreach( $images as $image ): ?>
																			<div class="slide">
																				<div class="slide-holder rooms">
																			
																					 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
																				
																			
																			
																				</div>
																			</div>
																		<?php endforeach; ?>
																
																<?php endif; ?>
																				
															</div>
														</div>
                                                        <a class="btn-prev" href="#"><i class="icon-arrow-left"></i></a>
														<a class="btn-next" href="#"><i class="icon-arrow-right"></i></a>
													</div>
														
											
											
												</section>
											
								
							
											<article class="post" >
													
												<?php the_sub_field("text"); ?>
											
											<div class="col-md-12"><a href="<?php the_field("room_booking_link"); ?>" class="button">Reserve A Room Now >></a></div
											</article>
							
									
								
									
										
							
								</div>
							</div>
						</div>
					</li>
                <?php endif; ?>
				<?php endwhile;?>
				<?php endif;?>
				
						
			</ul>
			
		</div>

	<?php wp_reset_postdata();?>
		
				</div>
			</div>
</div>

	
 