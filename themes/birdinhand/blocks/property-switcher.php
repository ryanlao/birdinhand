	<div class="col-md-2">
				<div class="col-md-12 col-sm-2 col-md-offset-1">
                	<?php $bih_switcher_logo = get_field('bih_feature_image_1'); ?>
					<img width="80%" style="opacity:0.35;" src="<?php if($bih_switcher_logo): echo $bih_switcher_logo; else: ?>http://birdinhand.wpengine.com/wp-content/uploads/2015/12/main-logo1.png<?php endif; ?>" />
					<div class="dropdown">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
						Select Property
						<span class="caret"></span>
					  </button>
 <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<li><a href="http://birdinhand.wpengine.com/bird-in-hand-family-inn/">Bird-in-Hand Family Inn</a></li>
							<li><a href="http://birdinhand.wpengine.com/bird-in-hand-village-inn-suites/">Bird-in-Hand Village Inn &amp; Suites</a></li>
							<li><a href="http://birdinhand.wpengine.com/amish-country-motel/">Amish Country Motel</a></li>
							<li><a href="http://birdinhand.wpengine.com/country-acres-campground/">Country Acres Campground</a></li>
							<li><a href="http://birdinhand.wpengine.com/travelers-rest-motel/">Travelers Rest Motel</a></li>

 </ul>
 		</div>
				</div>
			</div>	