<?php
	the_posts_pagination( array(
		'prev_text' => __( 'Previous page', 'birdinhand' ),
		'next_text' => __( 'Next page', 'birdinhand' ),
	) );
?>
