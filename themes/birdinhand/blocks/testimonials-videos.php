<?php if(get_field('side_video')): ?>
					<div class="col-md-4">
						<div class="embed-container">
					<?php the_field('side_video'); ?>
						</div>
					</div>	
					<?php elseif (get_field('testimonials')): ?>	
							<div class="col-md-4 meet-the-farmer">
						<p><?php the_field("farmer_intro"); ?></p>
						
							
						</div>
						<div class="img-wrap col-md-4">
							<?php $repeater = get_field( 'testimonials' );
								$rand = rand(0, (count($repeater) - 1));
								echo $repeater[$rand]['testimonial'];
							?>
						</div>
						
						
						
					
				
<?php endif; ?>	
<?php if(get_field('side_image')): ?>
					
					<?php elseif (get_field('testimonials')): ?>	
							<div class="col-md-4 meet-the-farmer">
						<p><?php the_field("farmer_intro"); ?></p>
						
							
						</div>
						<div class="img-wrap col-md-4 testimonial-holder">
							<?php $repeater = get_field( 'testimonials' );
$rand = rand(0, (count($repeater) - 1));
echo $repeater[$rand]['testimonial'];
?>
						</div>
						
						
						
					
				
<?php endif; ?>	
<script type="text/javascript">

</script>