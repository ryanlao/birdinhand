<?php  $property_intro = get_field('map_directions_intro'); ?>

	
		<div class="fixed-block side-menu">
		
			<ul class="list-holder">
            
            <?php if(have_rows('header_side_menu')):  global $post; ?>
            <?php while(have_rows('header_side_menu')) : the_row(); ?>
            
            	<?php 
					
					$header_side_menu_item_type = get_sub_field('header_side_menu_item_type');
					if($header_side_menu_item_type == 'Text Link'):
						?>
                        	
                            <li>
                                <a class="open" href="<?php the_sub_field('header_side_menu_link_url'); ?>">
                                    <i class="<?php the_sub_field()?>"></i>
                                    <span><?php the_sub_field('header_side_menu_link_text'); ?></span>
                                    <i class="fa <?php the_sub_field("header_side_menu_item_icon"); ?>"></i>
                                </a>
                            </li>
                            
                        <?php
					//endif link text type
					
					
					elseif($header_side_menu_item_type == 'Post Object'): 
					
						$header_side_menu_item_postID	= get_sub_field('header_side_menu_post_object');
						$post = get_post($header_side_menu_item_postID);
						setup_postdata($post);
                       

							
							?>
                
<?php $alternative_names = get_field('alternative_names'); ?>
                            	<li>
                                    
                                    <a <?php if (get_sub_field('hide_from_menu')):?> style="display: none;" <?php endif; ?>  data-popup="popup-<?php the_sub_field('header_side_menu_item_popupID'); ?>" class="open" href="#">
                                       <i class="fa <?php the_sub_field("header_side_menu_item_icon"); ?>"></i>
                                        <?php if($alternative_names == true && get_field('popup_menu_name')): ?>
                                            <span><?php the_field("popup_menu_name"); ?></span><i class="icon-arrow-right"></i></a>
                                        <?php else: ?>
                                            <?php the_title('<span>','</span>')?><i class="icon-arrow-right"></i></a>
                                        <?php endif; ?>
                                       
                                    <div class="popup">
                                        <div class="head-block">
                                        <a class="close" href="#">X</a>
                                            <?php if($popup_title=get_field("popup_title")):?>
                                                <h1><?php the_title(); ?></h1>
                                            <?php endif;?>
                                            
                                        </div>
                                        <div class="scroll-wrap">
                                            
                                            <div class="block jcf-scrollable">
                                                <?php if(has_post_thumbnail()):?>
                                                <?php
                                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1111x513' );
                                                ?>
                                                    <div class="img-area" style="background-image: url(<?php echo $image[0];?>);">
                                                        <strong class="title"><?php echo get_post_meta( get_post_thumbnail_id( get_the_ID()), '_wp_attachment_image_alt', true );?></strong>
                                                    </div>
                                                <?php endif;?>
                                                <?php if( have_rows('popup_blocks') ):?>
                                                    <?php while( have_rows('popup_blocks') ): the_row();?>
                                                        <article class="post">
                                                            <?php if($title=get_sub_field("title")):?>
                                                                <h1>
                                                                    <?php if($link=esc_url(get_sub_field("link"))):?>
                                                                        <a href="<?php echo $link?>">
                                                                    <?php endif;?>
                                                                            <?php echo $title;?>
                                                                    <?php if($link):?>
                                                                        </a>
                                                                    <?php endif;?>
                                                                </h1>
                                                                
                                                            <?php endif;?>
                                                            <?php the_sub_field("text");?>
                                                            <?php if($link):?>
                                                                <a href="<?php echo $link?>" class="link"><?php _e("learn more","birdinhand")?><i class="icon-angle-double-right"></i></a>
                                                            <?php endif;?>
                                                        </article>
                                                    <?php endwhile;?>
                                                <?php endif;?>
                                                <?php the_field("php_content"); ?>
                                                <div class="popup-intro">
                                                            <?php $postID = get_the_ID(); ?>
                                                            
                                                                
                                                            
                                                        
                                                    
                                                        <?php if ($postID == 1299): ?>
                                                            <div id="map_canvas"></div>
                                                        <?php endif; ?>
                                                        
                                                        <?php if ($postID == 1299): ?>
                                                            <div class="col-md-12" style="position:relative;top:300px;">
                                                                
                                                                    
                                                       
                                                                        <?php echo $property_intro; ?>
                                                            
                                                            </div>
                                                                    <?php else: ?>
                                                                    <div class="col-md-12">
                                                                        <?php the_content(); ?>
                                                                    </div>
                                                                    <?php endif; ?>
                                                                
                                                            
                                            
                                                                              <div class="col-md-12">
                                    <?php if( have_rows('tabs') ):?>
                                        <ul class="nav nav-tabs" role="tablist">
                                            <?php while( have_rows('tabs') ): the_row();?>
                                                <?php 
                                                        $tab_title_string = get_sub_field("tab_title");
                                                        $tab_id = preg_replace("/[^a-zA-Z0-9]/", "", $tab_title_string);
                                            
                                                    ?>
                                            <li role="presentation" class=" <?php the_sub_field("tab_state"); ?>"><a href="#<?php echo $tab_id; ?>" aria-controls="<?php echo $tab_id; ?>" role="tab" data-toggle="tab"><?php the_sub_field("tab_title"); ?></a></li>
                                            <?php endwhile; ?>
                                            </ul>
                                        <?php endif; ?>	
                                        
                                        
                                        <?php if( have_rows('tabs') ):?>
                                                
                                            <div class="tab-content">
                                                <?php while( have_rows('tabs') ): the_row();?>
                                                    <?php 
                                                        $tab_title_string = get_sub_field("tab_title");
                                                        $tab_id = preg_replace("/[^a-zA-Z0-9]/", "", $tab_title_string);
                                            
                                                    ?>
                                                    <div role="tabpanel" class="tab-pane <?php the_sub_field("tab_state"); ?>" id="<?php echo $tab_id; ?>"><?php the_sub_field("tab_content"); ?>
                                                        <?php if (get_field("menu_button_text")): ?>
                                                            <a class="button gform_button" href="<?php the_sub_field("menu_button"); ?>"><?php the_sub_field("menu_button_text"); ?></a>
                                                        <?php endif; ?>
                                                    </div>
                                                    
                                                <?php endwhile; ?>
                                            </div>
                                        <?php endif; ?>	
                                    </div>
                                                
                                                </div>
                                                <?php wp_reset_postdata();?>
                                                
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php
							
							
						wp_reset_postdata();
					
					//endif post object type
					
					
					elseif($header_side_menu_item_type == 'Specials'): 
					
						?>
								<?php
                                                    $specialsQuery = get_field('specials_experiences');
                                                    $specialsQueryString = implode(",", $specialsQuery);
                        ?>					
                        <li>
                        <a data-popup="popup-specials" class="open" href="#">
                        <i class="icon-dollar"></i>
                        <span>Packages & Special Experiences</span>
                        <i class="icon-arrow-right"></i></a>
                                    <div class="popup">
                                <div class="head-block">
                                <a class="close" href="#">X</a>
                                
                                        <h1>Packages & Special Experiences</h1>
                            
                                    
                                </div>
                                <div class="scroll-wrap">
                                    <a class="close" href="#"></a>
                                    <div class="block jcf-scrollable">
                                            <div class="row specials">
                                                <div class="col-md-12">
                                                    <div class="popup-intro">Some moments you’ll remember forever: eating barbecued chicken and steamed corn in the field where it was grown and harvested…. enjoying a private wine tasting for two and a romantic get-away at our historic country inn…. staying in our Schoolhouse Suite....going behind the scenes at an Amish farm during harvest time…. Not only will you make great memories and find lots of things to do in Bird-in-Hand, but you can save money, too. 
                                                    
                                                    
                                                    </div>
                                                
                                                </div>
                                                
                                                <?php
                                                    
                                                    $catquery = new WP_Query( 'category_name=' . $specialsQueryString . '&posts_per_page=10' );
                                                    
                                                    while($catquery->have_posts()) : $catquery->the_post();
                                                    ?>
                                                    
                                                        <div class="col-md-4 single-special box
                                                                <?php foreach(get_the_category() as $category) {
                                                                echo $category->slug . ' ';} ?>
                                                                    
                                                        "> <!-- closes the class -->
                                                            <a href="<?php the_permalink() ?>" rel="bookmark">
                                                                
                                                                    <?php 
                                                                        
                                                                            the_post_thumbnail('thumbnail_260x265');
                                                                            
                                                                        ?>
                                                                <div class="specials-title-holder">
                                                                    <h3 class="specials-title"><?php the_title(); ?></h3>	
                                                                </div>	
                                                            </a>
                                                            <div class="location-holder">
                                                                Available at: <?php foreach((get_the_category()) as $cat) {
                                                                    if (!($cat->cat_name=='packages')) echo $cat->cat_name . ' '; 
                                                                    } 
                                                                ?>	
                                                            </div>
                                                                
                                                        </div>
                                                        
                                                    
                                                    
                                                <?php endwhile; ?>
                                            </div>
                                        
                                            
                                    </div>
                                </div>
                            </div>
                            </li>	
                        <?php
					
					//endif specials type
										
					
					elseif($header_side_menu_item_type == 'schedulepopup'):
					
						?>
                        	
								<li>	
									<a data-popup="popup-schedulepopup" class="open" href="#">
										<i class="icon-calendar"></i>
										<span><?php the_sub_field('header_side_menu_link_text') ?></span>
										<i class="icon-arrow-right"></i></a>
											
								<div class="popup">
									<div class="head-block">
									<a class="close" href="#">X</a>
										<h1><?php the_sub_field('header_side_menu_link_text') ?></h1>
									
										
									</div>
									<div class="scroll-wrap">
										
										<div class="block jcf-scrollable shows-scroll">	
										<?php if( have_rows('call_show_events') ):?>
										<?php while( have_rows('call_show_events') ): the_row();?>
											<div class="col-md-3">
												<h2><?php the_sub_field("show_title"); ?></h2>
												<div class="<?php the_sub_field("show_code"); ?>"></div>
											</div>
										<?php endwhile; ?>
										<?php endif; ?>

										</div>
									</div>			
													
								</li>				
							
                        <?php
					
					endif; // end if header_side_menu_item_type
				
				?>
                
                <?php endwhile; ?>
                <?php endif; //acf have rows loop end ?>				
			</ul>
			
		</div>

	<?php wp_reset_postdata();?>

	
 