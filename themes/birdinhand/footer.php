<footer id="footer">
    <?php if( have_rows('sections') && !is_front_page() ):?>
			<?php while( have_rows('sections') ): the_row();?>
				<div class="row" >
					<div class="col-md-12 box-wrap box">
						<div class="col-md-7 property-img <?php if(!get_sub_field("picture_left")):?> pull-right<?php endif;?>"
							<?php if($image=get_sub_field("image")):?>
								style="background-image: url('<?php echo $image;?>');"
							<?php endif;?>>
							<div class="text-holder">
								<?php if($image_title=get_sub_field("image_title")):?>
									<strong class="title"><?php echo $image_title;?></strong>
								<?php endif;?>
								
							</div>
						</div>
						<div class="col-md-5 <?php if(get_sub_field("picture_left")):?> first-row<?php endif;?>">
							<div class="homepage-col-text">
								<?php if($title=get_sub_field("title")):?>
									<h2><?php echo $title;?></h2>
								<?php endif;?>
								<?php the_sub_field("text");?>
								<?php if($button_text=get_sub_field("button_text")):?>
										<a href="<?php the_sub_field("link")?>" class="button"><?php echo $button_text?></a>
									<?php endif;?>
							</div>
						</div>
					</div>
				</div>	
			<?php endwhile;?>
		<?php endif;?>



    <?php if(have_rows('sections_copy')) : ?>
				<?php while( have_rows('sections_copy') ): the_row();?>
				<div class="row">
					<div class="col-md-12 box-wrap box">
						<div class="col-md-7 property-img <?php if(!get_sub_field("picture_left")):?> pull-right<?php endif;?>"
							<?php if($image=get_sub_field("image")):?>
								style="background-image: url('<?php echo $image;?>');"
							<?php endif;?>
							>
					
							<div class="text-holder">
								<?php if($image_title=get_sub_field("image_title")):?>
									<strong class="title"><?php echo $image_title;?></strong>
								<?php endif;?>
								
							</div>
						</div>
						<div class="col-md-5 <?php if(get_sub_field("picture_left")):?> first-row<?php endif;?>">
							<div class="homepage-col-text">
								<?php if($title=get_sub_field("title")):?>
									<h2><?php echo $title;?></h2>
								<?php endif;?>
								<?php the_sub_field("text");?>
								<?php if($button_text=get_sub_field("button_text")):?>
										<a href="<?php the_sub_field("link")?>" class="button"><?php echo $button_text?></a>
									<?php endif;?>
							</div>
						</div>
					</div>
				</div>	
			<?php endwhile;?>
		<?php endif;?>
		<div class="container" style="padding-top:40px;">
		<div class="row">
		<div class="col-md-12">
		<?php if ( is_page( '23' )) {    
?> 
<?php echo do_shortcode('[yith_wc_productslider id=2151]'); ?>
<?php } ?>
</div></div></div>
		                <div class="row" style="background-color:#eaeaea; padding: 30px 55px 15px 55px;">
                    <a href="#" class="back-to-top icon-arrow-top"></a>
<?php   $column_numbers = count( get_field('footer_columns') );
        if ($column_numbers == 1):
        $bootstrap_columns = 6;
        $offset_columns = 3;
        elseif ($column_numbers == 2):
        $bootstrap_columns = 6;
        $offset_columns = 0;
        elseif ($column_numbers == 3):
        $bootstrap_columns = 4;
        endif;
         ?>

    <?php if( have_rows('footer_columns') ): ?>

    <?php while( have_rows('footer_columns') ): the_row();?>
    <div class="footer-columns-mid col-md-<?php echo $bootstrap_columns; ?> col-md-offset-<?php echo $offset_columns; ?>">
        <?php the_sub_field("Column")?>
    </div>

    <?php endwhile; ?>

    <?php endif; ?>
<div style="clear:both;"></div>
</div>
			<div class="">

                
			
                		

    
                <a name="footer-link"></a>
				<div class="col-block">
					<div class="container">
					<div class="col">
                         		<?php echo do_shortcode(get_field("subscribe_form","options"));?>
						<?php echo do_shortcode(get_field("gravity_form","options"));?>
				

                              

						      
						<?php if( have_rows('social_dropdowns') ): ?>
                                    <?php while( have_rows('social_dropdowns') ): the_row();?>
               
                                         <?php   $image = get_sub_field('image'); ?>
		                               
		                                        <div class="social-net dropdown">
                                                  <a class="fa <?php echo $image; ?>" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                       </a>
                                                     <?php if( have_rows('social_links') ): ?>
                                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <?php while( have_rows('social_links') ): the_row();?>
                                             <?php   $link = get_sub_field('link'); ?>
                                                <?php   $property = get_sub_field('property'); ?>
                                                    
                                                 
                                                  
                                                    <li><a class="property-social-link" target="_blank" href="<?php echo $link; ?>"><?php echo $property; ?></a></li>
                                                  
                                                

                                            <?php endwhile;?>
                                                        </ul>
                                        <?php endif; ?>
                                                    </div>
                                    <?php endwhile;?>
						<?php endif; ?>

						<?php if(!have_rows('social_dropdowns') ): ?>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-facebook"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu">
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayAmishCountry">Amish Country
                Motel</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/EatBIHBakery">Bird-in-Hand Bakery
                &amp; Cafe</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayBIHFamilyInn/">Bird-in-Hand
                Family Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/EnjoyBIHRestaurantStage">Bird-in-Hand
                Family Restaurant &amp; Smorgasbord</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/BirdInHandStage">Bird-in-Hand
                Stage</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayBIHVillageInn">Bird-in-Hand
                Village Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayCountryAcres">Country Acres
                Campground</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayTravelersRest">Travelers Rest
                Motel</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-twitter"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu" style=
        "display: none;">
            <li>
                <a class="property-social-link" href=
                "https://twitter.com/BirdinHandNews">Bird-in-Hand</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-google-plus"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu">
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/108711304751365443917/posts">Amish
                Country Motel</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/108612624412987899327/posts">Bird-in-Hand
                Bakery &amp; Cafe</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/116608081945696114872/posts">Bird-in-Hand
                Family Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/100596160016055074734/posts">Bird-in-Hand
                Family Restaurant &amp; Smorgasbord</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/109285815040234906036/posts">Bird-in-Hand
                Stage</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/114076772192103625031/posts">Bird-in-Hand
                Village Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/109935983771991635595/posts">Country
                Acres Campground</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/103298858345041402585/posts">Travelers
                Rest Motel</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-youtube"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu" style=
        "display: none;">
            <li>
                <a class="property-social-link" href=
                "https://www.youtube.com/channel/UC02Inlavo6OYDW2nEn53qtQ">Bird-in-Hand</a>
            </li>
        </ul>
    </div>
						<?php endif; ?>
                 
					</div>
					<div class="col footimg">
						<img src="<?php echo get_template_directory_uri()?>/images/logo2.png" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
					</div>
					<?php
					  if(get_field("tel"))$tel=get_field("tel");
					  elseif(get_field("tel","options"))$tel=get_field("tel","options");
					  if(get_field("adsress_text"))$adsress_text=get_field("adsress_text");
					  elseif(get_field("adsress_text","options"))$adsress_text=get_field("adsress_text","options");
					  if(get_field("email"))$email=antispambot(get_field("email"));
					  elseif(get_field("email","options"))$email=antispambot(get_field("email","options"));
					?>
					<?php if($tel or $adsress_text or $email):?>
					<div class="col">
						<h3><?php _e("Contact us","birdinhand")?></h3>
						<ul class="contact-info">
							<?php if($adsress_text):?>
							  <li><i class="icon-location"></i><address><?php echo $adsress_text?></address></li>
							<?php endif;?>
							<?php if($tel):?>
							  <li><i class="icon-phone2"></i><a href="tel:<?php echo clean_phone($tel)?>"><?php echo $tel?></a></li>
							<?php endif;?>
							<?php if($email):?>
							  <li><i class="icon-mail2"></i><a href="mailto:<?php echo $email?>"><?php echo $email?></a></li>
							<?php endif;?>
						</ul>
					</div>
					<?php endif;?>
				 <div style="clear:both;"></div>
				</div>
				</div>
<div class="container">
				<div class="copyright">
				
					<p><?php _e("Copyright","birdinhand")?> &copy; <a href="#"><?php _e("Bird-in-Hand","birdinhand")?></a>.<span><?php _e("All Rights Reserved","birdinhand")?></span>. </p>
				</div>
				</div>
			</div>
		</footer>
	</div>

	<?php wp_deregister_style( 'gforms_formsmain_css');?>
	<?php wp_footer(); ?>
	
	<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry"></script>
			<?php if (get_field('display_map') == 'SuppliersMap'): ?>
			<script>jQuery('.logo').addClass('scrolled');</script>
		<script src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map1Data.js"></script>
	<script src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map.js"></script>
	
	<?php elseif (get_field('display_map') == 'BIHMap'): ?>
<script>jQuery('.logo').addClass('scrolled');</script>
		<script src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map2Data.js"></script>
	<script src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map-contact.js"></script>
	<?php else: ?>
	
		<script src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map2Data.js"></script>
	<script src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map-contact.js"></script>
	<?php endif; ?>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.3.0/jquery-migrate.min.js"></script>
         <script type="text/javascript" src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/res-assets/ew_js/jquery-ui-1.8.9.custom.min.js"></script>

<script type="text/javascript">


// Fix Social Footer on pages with only one link in the dropdown
jQuery('.social-net').each(function(){
	var linkVal = jQuery(this).find('.dropdown-menu li');
	var linkTot = linkVal.length;
	if(linkTot <= 1) { 
        jQuery(this).addClass('single');
	};
});

jQuery('.social-net.single').each(function(){
    var linkUrl = jQuery(this).find('.dropdown-menu li a').attr('href');
    jQuery(this).find('.dropdown-menu').hide();
    jQuery(this).click(function(){
        window.open(linkUrl);
    });
})

// Line under H1 in Hero Image
jQuery('.banner h1').each(function() {
         if (!jQuery(this).text() == "") {
                   jQuery('.banner strong.title').addClass('lined')
         }
});
if (jQuery('.banner .title').text() == "") {
       jQuery('.banner strong.title').removeClass('lined')
}

// Fix H1 in Popups
jQuery('.popup.caamenities .popup-content-holder h1').appendTo('.popup.caamenities .head-block');
jQuery('.popup.tramenities .popup-content-holder h1').appendTo('.popup.tramenities .head-block');
jQuery('.popup.groups .popup-content-holder h1').appendTo('.popup.groups .head-block');
jQuery('.popup.groupsstage .popup-content-holder h1').appendTo('.popup.groupsstage .head-block');
jQuery('.tribe-events-event-url a').html('Website');




/* Specials Split Rows so margins don't knock it to 3 in a row
var divs = jQuery(".page-template-template-specials .box.col-md-3.col-xs-12");
for(var i = 0; i < divs.length; i+=4) {
  divs.slice(i, i+4).wrapAll("<div class='clearfix'></div>");
}
*/
var divs = jQuery(".page-template-template-specials .box.col-md-3.col-xs-12");
var maxHeight = 0;
jQuery(divs).each(function(){
   maxHeight = jQuery(this).height() > maxHeight ? jQuery(this).height() : maxHeight;
});
divs.css("height", maxHeight);

// Rooms Row Fix
if(jQuery('.new-width').length > 3) { 
    jQuery('.new-width').css("width", "25%");
}

// Meet the Farmers Fix
var farmFix = jQuery(".property-box.property-information.container > .row > .col-md-12 > .col-md-3");
for(var i = 0; i < farmFix.length; i+=4) {
  farmFix.slice(i, i+4).wrapAll("<div class='clearfix'></div>");
}

// Products Navigation Fix
jQuery("<div class='clearfix'></div>").insertAfter('.woocommerce .products');



// Footer Padding Removal (If No Buttons Exist)
if(!jQuery('#footer > .row a').hasClass('button')) { 
    jQuery('#footer > .row').addClass('padless');
}

// Cart Button on Single Product Page
jQuery('.woocommerce.single .cart-container').prependTo('.row.top-100')

if(jQuery(window).width() < 991) {
    var lowdivs = jQuery(".lower-images h2");
    var lowmaxHeight = 0;
    jQuery(lowdivs).each(function(){
       lowmaxHeight = jQuery(this).height() > lowmaxHeight ? jQuery(this).height() : lowmaxHeight;
    });
    lowdivs.css("height", lowmaxHeight + 50);
}

jQuery('li a[href="#Breakfast"]').click(function(){
    jQuery('.tab-pane').removeClass("active");
    jQuery('.tab-pane#Breakfast').removeClass("inactive").addClass("active");
	
})

</script>
<script type='text/javascript'>
(function (d, t) {
  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
  bh.type = 'text/javascript';
  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=wfvhwj6g821cjmsemgqyyq';
  s.parentNode.insertBefore(bh, s);
  })(document, 'script');
</script>

</body>
</html>