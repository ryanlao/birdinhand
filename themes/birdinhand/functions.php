<?php

//Staging restrictions
if ( file_exists( sys_get_temp_dir() . '/staging-restrictions.php' ) ) {
	define( 'STAGING_RESTRICTIONS', true );
	require_once sys_get_temp_dir() . '/staging-restrictions.php';
}

function seo_warning() {
	if( get_option( 'blog_public' ) ) return;
	
	$message = __( 'You are blocking access to robots. You must go to your <a href="%s">Reading</a> settings and uncheck the box for Search Engine Visibility.', 'birdinhand' );

	echo '<div class="error"><p>';
	printf( $message, admin_url( 'options-reading.php' ) );
	echo '</p></div>';
}
add_action( 'admin_notices', 'seo_warning' );


add_action( 'themecheck_checks_loaded', 'theme_disable_cheks' );
function theme_disable_cheks() {
	$disabled_checks = array( 'TagCheck', 'Plugin_Territory', 'CustomCheck', 'EditorStyleCheck' );
	global $themechecks;
	foreach ( $themechecks as $key => $check ) {
		if ( is_object( $check ) && in_array( get_class( $check ), $disabled_checks ) ) {
			unset( $themechecks[$key] );
		}
	}
}

add_theme_support( 'automatic-feed-links' );

if ( !isset( $content_width ) ) {
	$content_width = 900;
}
// Show featured images
function custom_widget_featured_image() {
	global $post;

	echo tribe_event_featured_image( $post->ID, 'thumbnail' );
}


// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

// dequeue ajax/jQuery (conflicts with existing js for BIH theme)
function dequeue_tribe_events_scripts_and_styles() {

wp_dequeue_script( 'tribe-events-list' );

}
add_action('wp_enqueue_scripts', 'dequeue_tribe_events_scripts_and_styles', 100 );
remove_action( 'wp_head', 'wp_generator' );

add_action( 'after_setup_theme', 'theme_localization' );
function theme_localization () {
	load_theme_textdomain( 'birdinhand', get_template_directory() . '/languages' );
}

// Hide events that are in the past

add_filter('tribe_events_pre_get_posts', 'filter_tribe_all_occurences', 100);
 
function filter_tribe_all_occurences ($wp_query) {
 
        if ( !is_admin() )  {
 
                $new_meta = array();
                $today = new DateTime();
 
                // Join with existing meta_query
                if(is_array($wp_query->meta_query))
                        $new_meta = $wp_query->meta_query;
 
                // Add new meta_query, select events ending from now forward
                $new_meta[] = array(
                        'key' => '_EventEndDate',
                        'type' => 'DATETIME',
                        'compare' => '>=',
                        'value' => $today->format('Y-m-d H:i:s')
                );
 
                $wp_query->set( 'meta_query', $new_meta );
        }
 
        return $wp_query;
}
/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );


add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
add_image_size( 'thumbnail_400x9999', 400, 9999, true );
add_image_size( 'thumbnail_1111x513', 1111, 513, true );
add_image_size( 'thumbnail_548x351', 548, 351, true );
add_image_size( 'thumbnail_367x335', 367, 335, true );
add_image_size( 'thumbnail_557x363', 557, 363, true );
add_image_size( 'thumbnail_1042x589', 1042, 589, true );
add_image_size( 'thumbnail_260x265', 260, 265, true );
add_image_size( 'thumbnail_1680x896', 1680, 896, true );
add_image_size( 'thumbnail_152x140', 152, 140, true );
add_image_size( 'thumbnail_579x439', 579, 439, true );
add_image_size( 'thumbnail_400x280', 400, 280, true );

function bih_get_image_width() {

    $thumb_id   = get_post_thumbnail_id();
    $image_data = wp_get_attachment_image_src( $thumb_id , 'tp-thumbnail' );

    $width  = $image_data[1];
    $height = $image_data[2];

    if ( $width > 1000 ) {
        return 'wide enough';
    }
}
register_nav_menus( array(
	'primary_left' => __( 'Primary left Navigation', 'birdinhand' ),
	'primary_right' => __( 'Primary right Navigation', 'birdinhand' ),
) );


//Add [email]...[/email] shortcode
function shortcode_email( $atts, $content ) {
	return antispambot( $content );
}
add_shortcode( 'email', 'shortcode_email' );

//Register tag [template-url]
function filter_template_url( $text ) {
	return str_replace( '[template-url]', get_template_directory_uri(), $text );
}
add_filter( 'the_content', 'filter_template_url' );
add_filter( 'widget_text', 'filter_template_url' );

//Register tag [site-url]
function filter_site_url( $text ) {
	return str_replace( '[site-url]', home_url(), $text );
}
add_filter( 'the_content', 'filter_site_url' );
add_filter( 'widget_text', 'filter_site_url' );

if( class_exists( 'acf' ) && !is_admin() ) {
	add_filter( 'acf/load_value', 'filter_template_url' );
	add_filter( 'acf/load_value', 'filter_site_url' );
}

//Replace standard wp menu classes
function change_menu_classes( $css_classes ) {
	return str_replace( array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' ), 'active', $css_classes );
}
add_filter( 'nav_menu_css_class', 'change_menu_classes' );

//Allow tags in category description
$filters = array( 'pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description' );
foreach ( $filters as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
}

function clean_phone( $phone ){
    return preg_replace( '/[^0-9]/', '', $phone );
}

//Make wp admin menu html valid
function wp_admin_bar_valid_search_menu( $wp_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch"><div>';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" tabindex="10" type="text" value="" maxlength="150" />';
	$form .= '<input type="submit" class="adminbar-button" value="' . __( 'Search', 'birdinhand' ) . '"/>';
	$form .= '</div></form>';

	$wp_admin_bar->add_menu( array(
		'parent' => 'top-secondary',
		'id'     => 'search',
		'title'  => $form,
		'meta'   => array(
			'class'    => 'admin-bar-search',
			'tabindex' => -1,
		)
	) );
}

function fix_admin_menu_search() {
	remove_action( 'admin_bar_menu', 'wp_admin_bar_search_menu', 4 );
	add_action( 'admin_bar_menu', 'wp_admin_bar_valid_search_menu', 4 );
}
add_action( 'add_admin_bar_menus', 'fix_admin_menu_search' );

//Disable comments on pages by default
function theme_page_comment_status( $post_ID, $post, $update ) {
	if ( !$update ) {
		remove_action( 'save_post_page', 'theme_page_comment_status', 10 );
		wp_update_post( array(
			'ID' => $post->ID,
			'comment_status' => 'closed',
		) );
		add_action( 'save_post_page', 'theme_page_comment_status', 10, 3 );
	}
}
add_action( 'save_post_page', 'theme_page_comment_status', 10, 3 );

//custom excerpt
function theme_the_excerpt() {
	global $post;
	
	if ( trim( $post->post_excerpt ) ) {
		the_excerpt();
	} elseif ( strpos( $post->post_content, '<!--more-->' ) !== false ) {
		the_content();
	} else {
		the_excerpt();
	}
}


// Hide Shows from main calendar and list 

add_action( 'pre_get_posts', 'tribe_exclude_events_category_month_list' );
function tribe_exclude_events_category_month_list( $query ) {

	if ( isset( $query->query_vars['eventDisplay'] ) && ! is_singular( 'tribe_events' ) ) {

		if ( $query->query_vars['eventDisplay'] == 'list' && ! is_tax( Tribe__Events__Main::TAXONOMY ) || $query->query_vars['eventDisplay'] == 'month' && $query->query_vars['post_type'] == Tribe__Events__Main::POSTTYPE && ! is_tax( Tribe__Events__Main::TAXONOMY ) && empty( $query->query_vars['suppress_filters'] ) ) {

			$query->set( 'tax_query', array(

				array(
					'taxonomy' => Tribe__Events__Main::TAXONOMY,
					'field'    => 'slug',
					'terms'    => array( 'stage' ),
					'operator' => 'NOT IN'
				)
			) );
		}

	}

	return $query;
}

//theme password form
function theme_get_the_password_form() {
	global $post;
	$post = get_post( $post );
	$label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
	$output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
	<p>' . __( 'This content is password protected. To view it please enter your password below:', 'birdinhand' ) . '</p>
	<p><label for="' . $label . '">' . __( 'Password:', 'birdinhand' ) . '</label> <input name="post_password" id="' . $label . '" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr__( 'Submit', 'birdinhand' ) . '" /></p></form>
	';
	return $output;
}
add_filter( 'the_password_form', 'theme_get_the_password_form' );

function base_scripts_styles() {
	$in_footer = true;
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	wp_deregister_script( 'comment-reply' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply', get_template_directory_uri() . '/js/comment-reply.js', '', '', $in_footer );
	}
	
	// Loads JavaScript file with functionality specific.
	wp_enqueue_script( 'base-script', get_template_directory_uri() . '/js/jquery.main.js', array( 'jquery' ), '', $in_footer );
	
	//theme js
	wp_enqueue_script( 'bih-main', get_template_directory_uri() . '/js/birdinhand-main.js', array(), '', true );
	
	// Loads Bootstrap jQuery for Tabs
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '', $in_footer );
	
	// Loads in Bootstrap grid and Tabs only
	wp_enqueue_style( 'base-theme', get_template_directory_uri() . '/css/bootstrap.min.css', array() );

	// Loads our main stylesheet.
	wp_enqueue_style( 'base-style', get_stylesheet_uri(), array() );
	
	wp_enqueue_style( 'base-font', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic%7COpen+Sans:400,600,700,600italic,400italic%7CEB+Garamond', array() );	
	
	// Implementation stylesheet.
	wp_enqueue_style( 'base-theme', get_template_directory_uri() . '/theme.css', array() );
	
	
}
add_action( 'wp_enqueue_scripts', 'base_scripts_styles' );

add_action( 'admin_init', 'basetheme_options_capability' );
function basetheme_options_capability(){
	$role = get_role( 'administrator' );
	$role->add_cap( 'theme_options_view' );
}

//theme options tab in appearance
if( function_exists( 'acf_add_options_sub_page' ) && current_user_can( 'theme_options_view' ) ) {
	acf_add_options_sub_page( array(
		'title'  => 'Theme Options',
		'parent' => 'themes.php',
	) );
}

//WooCommerce Product Import
add_filter( 'woocommerce_csv_import_limit_per_request', create_function( '', 'return 200;' ) ); //Increase limit to 100
//WooCommerce Compatibility Fixes
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

add_filter( 'woocommerce_after_main_content' , 'shipping_terms' );
function shipping_terms() {
   echo '<div class="row"><div class="col-md-12" style="text-align:center;">SHIPPING POLICES CAN GO HERE</div></div>';
}

function my_theme_wrapper_start() {
  echo '<section class="row top-100">';
  echo '<div class="col-md-10 col-md-offset-1">';
}

function my_theme_wrapper_end() {
  echo '</div">';		
  echo '</section>';
}

//acf theme functions placeholders
if( !class_exists( 'acf' ) && !is_admin() ) {
	function get_field_reference( $field_name, $post_id ) { return ''; }
	function get_field_objects( $post_id = false, $options = array() ) { return false; }
	function get_fields( $post_id = false ) { return false; }
	function get_field( $field_key, $post_id = false, $format_value = true )  { return false; }
	function get_field_object( $field_key, $post_id = false, $options = array() ) { return false; }
	function the_field( $field_name, $post_id = false ) {}
	function have_rows( $field_name, $post_id = false ) { return false; }
	function the_row() {}
	function reset_rows( $hard_reset = false ) {}
	function has_sub_field( $field_name, $post_id = false ) { return false; }
	function get_sub_field( $field_name ) { return false; }
	function the_sub_field( $field_name ) {}
	function get_sub_field_object( $child_name ) { return false;}
	function acf_get_child_field_from_parent_field( $child_name, $parent ) { return false; }
	function register_field_group( $array ) {}
	function get_row_layout() { return false; }
	function acf_form_head() {}
	function acf_form( $options = array() ) {}
	function update_field( $field_key, $value, $post_id = false ) { return false; }
	function delete_field( $field_name, $post_id ) {}
	function create_field( $field ) {}
	function reset_the_repeater_field() {}
	function the_repeater_field( $field_name, $post_id = false ) { return false; }
	function the_flexible_field( $field_name, $post_id = false ) { return false; }
	function acf_filter_post_id( $post_id ) { return $post_id; }
}
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}



// theme js

function birdinhand_scripts() {
   
   wp_enqueue_script( 'juic', get_template_directory_uri() . '/js/jquery-ui-1.10.3.custom.min.js', array(), '', true );
   wp_enqueue_script( 'jmousewheel', get_template_directory_uri() . '/js/jquery.mousewheel.min.js', array(), '', true );
   wp_enqueue_script( 'jkinetic', get_template_directory_uri() . '/js/jquery.kinetic.min.js', array(), '', true );
   wp_enqueue_script( 'smoothdivscroll', get_template_directory_uri() . '/js/jquery.smoothdivscroll-1.3-min.js', array(), '', true );
   
}
add_action( 'wp_enqueue_scripts', 'birdinhand_scripts' );