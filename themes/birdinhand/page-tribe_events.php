<?php /*
	Template Name: Tribe Events
*/ ?>


<?php get_header(); ?>

<?php get_template_part( 'blocks/events-packages-intro'); ?>
<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<?php 
		$top_img_bg  = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	?>

    <?php 
		$top_img_bg  = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
        echo $top_img_bg;
	?>

	<div class="top-img-bg title" style="background-image:url('http://birdinhand.wpengine.com/wp-content/themes/birdinhand/images/Balloons1.jpg');">
			
				<h1>Take It All In</h1>
	
	</div>
   <?php if (is_archive()):?><div class="top-img-bg-push" style="margin-bottom:0px !important"></div><?php endif;?>
    
	<?php wp_link_pages(); ?>

</div>

<?php tribe_get_template_part( 'modules/bar' ); ?>
		<main id="main" role="main">
		<div class="container <?php if (!is_archive()):?>an-event<?php endif; ?>">
            <?php if (is_archive()):?>
          
            <?php endif; ?>
	
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_title( '<div class="title"><h1>', '</h1></div>' ); ?>
					<?php the_post_thumbnail( 'full' ); ?>
					<?php the_content(); ?>
					<?php edit_post_link( __( 'Edit', 'birdinhand' ) ); ?>
				<?php endwhile; ?>
				<?php wp_link_pages(); ?>
				<?php comments_template(); ?>
		</div>
		</main>

<?php get_footer(); ?>
