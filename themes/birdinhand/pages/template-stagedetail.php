<?php
/*
Template Name: Stage Detail Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" class="stage" role="main">
		<div class="banner inner-block"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		> <!-- This LT symbol Closes Banner -->
				<article class="text-block inner-block">
			
					<strong class="title"><?php the_field("uptitle"); ?></strong>
				
				<h1><?php the_field("main_title"); ?><h1>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php if($certificate_logo=get_field("certificate_logo")):?>
				<div class="image-block">
					<?php the_field("certificate_logo");?>
				</div>
			<?php endif;?>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
			<?php get_template_part('blocks/reservations'); ?>
			<?php if( have_rows('cols') ):?>	
		<div class="row">
			<div class="four-col">
				<?php while( have_rows('cols') ): the_row();?>
					<div class="col col-md-3">
						<?php if($image=get_sub_field("image")):?>
							<img src="<?php echo $image["sizes"]["thumbnail_548x351"];?>" alt="<?php echo $image["alt"];?>">
						<?php endif;?>
					
							<?php
								if(get_sub_field("color")=="yellow")$class=" green";
								elseif(get_sub_field("color")=="dark")$class=" brown";
								else $class="";
							
							?>
							<a href="<?php the_sub_field('page_link'); ?>" class="text-box <?php echo $class?>">
					
							<?php if($title=get_sub_field("title")):?>
								<strong class="title"><?php echo $title;?></strong>
							<?php endif;?>
							<?php if($sub_title=get_sub_field("sub_title")):?>
								<span class="category-title"><?php echo $sub_title;?></span>
							<?php endif;?>
						
							</a>
					
					
					</div>
				<?php endwhile; ?>
			</div>
		</div>	
	<?php endif;?>
		<div class="block-wrap">
		
						<?php get_template_part( 'blocks/property-switcher'); ?>
				
						
			<div class="text-wrap col-md-6">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>
		<?php if( have_rows('blocks') ):?>
			<div class="container">
				<div class="col-wrap">
					<?php while( have_rows('blocks') ): the_row();?>
						<div class="col">
							<div class="col-area">
								<?php if($image=get_sub_field("image")):?>
									<img src="<?php echo $image["sizes"]["thumbnail_400x280"];?>" alt="<?php echo $image["alt"];?>">
								<?php endif;?>
								<div class="text-block">
									<?php if($title=get_sub_field("name")):?>
										<h2><?php echo $title;?></h2>
									<?php endif;?>
									<?php the_sub_field("text");?>
								</div>
							</div>
						</div>
					<?php endwhile;?>
				</div>
			</div>
		<?php endif;?>
	
			<?php
			if($related=get_field("post_on_home")):
			$args = array (
			'ignore_sticky_posts'	=> 1,
			'post_type'				=> 'post',
			'posts_per_page'		=> '-1',
			'post__in'				=> $related,
			'orderby'				=> 'post__in',
			);
			$the_query = new WP_Query( $args );
		?>
		<?php if ($the_query->have_posts()) : ?>
			<section class="carousel-box">
				<?php if($carousel_title=get_field("carousel_title")):?>
					<h1><?php echo $carousel_title;?></h1>
				<?php endif;?>
				<div class="carousel">
					<div class="mask">
						<div class="slideset">
							<?php $i=0;while ($the_query->have_posts()) : $the_query->the_post();$i++ ?>
								<?php if($i%4==1):?><div class="slide"><?php endif;?>
									<div class="slide-holder">
										<a href="<?php the_permalink()?>" class="bg-cover">
											<?php the_post_thumbnail("thumbnail_260x265");?>
											<div class="text-area">
												<strong class="sub-title"><em><?php _e("View","birdinhand")?></em> <?php _e("Details","birdinhand")?><i class="icon-arrow"></i></strong>
											</div>
										</a>
										<?php the_title('<strong class="title">','</strong>');?>
									</div>
								<?php if($i%4==0):?></div><?php endif;?>
							<?php endwhile;?>
							<?php if($i%4!=0):?></div><?php endif;?>
						</div>
					</div>
					<a class="btn-prev" href="#"><i class="icon-arrow-left"></i></a>
					<a class="btn-next" href="#"><i class="icon-arrow-right"></i></a>
				</div>
			</section>
		<?php endif;?>
		<?php wp_reset_postdata();?>
		<?php endif;?>
		<div class="form-holder">
			<div class="container">
				<div class="block">
					<?php if($gravity_form=get_field("inner_gravity_form")):?>
						<div class="col">
							<?php echo do_shortcode($gravity_form);?>
						</div>
					<?php endif;?>
					<?php if( have_rows('cols') ):?>
						<?php while( have_rows('cols') ): the_row();?>
							<div class="col">
								<?php if($title=get_sub_field("title")):?>
									<h3><?php echo $title;?></h3>
								<?php endif;?>
								<?php if($link=esc_url(get_sub_field("link"))):?>
									<a href="<?php echo $link;?>" class="button"><?php _e("Learn More","birdinhand")?></a>
								<?php endif;?>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>