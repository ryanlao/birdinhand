<?php
/*
Template Name: Experiences and Packages Main Layout
*/
get_header(); ?>

		<main id="main" role="main">
         <div class="banner"
			    <?php if(has_post_thumbnail()):?>
				    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				    style="background-image: url(<?php echo $image[0];?>);"
			    <?php endif;?>
		    >
			    <article class="text-block inner-block">
				    <?php if($uptitle=get_field("uptitle")):?>
					    <strong class="title"><?php echo $uptitle;?></strong>
				    <?php endif;?>
				    <h1><?php the_field("main_title"); ?><h1>
				    <?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					    <a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				    <?php endif;?>
			    </article>
			    <?php get_template_part( 'blocks/popups'); ?>
		</div>
<?php get_template_part( 'blocks/reservations'); ?>
            			<div class="block-wrap">
		
						
				
						
			<div class="text-wrap col-md-10 col-md-offset-1">
				<?php the_content();?>
				
			</div>
					
		</div>
				<div class="col-md-10 col-md-offset-1 " style="margin-bottom: 15px;">
					<select class="btn-default">
						<option>Filter By Location</option>
						<option value="amish-country-motel">Amish Country Motel</option>
						<option value="bakery-cafe">Bakery & Cafe</option>
						<option value="country-acres-campground">Country Acres Campground</option>
						<option value="family-inn-suites">Family Inn</option>
						<option value="restaurant-smorgasbord">Restaurant & Smorgasbord</option>
						<option value="travelers-rest-motel">Travelers Rest Motel</option>
						<option value="village-inn-suites">Village Inn & Suites</option>
					</select>
				</div>
			
		<div class="col-md-10 col-md-offset-1">

			<?php
				$catquery = new WP_Query( 'category_name=bakery-cafe,restaurant-smorgasbord,restaurant-smorgasbord-groups-only,country-acres-campground,amish-country-motel,travelers-rest-motel,family-inn-suites,village-inn-suites&posts_per_page=20' );
				while($catquery->have_posts()) : $catquery->the_post();
				?>
				
					<div class="col-md-3 col-sm-12 col-xs-12 box
							<?php foreach(get_the_category() as $category) {
							echo $category->slug . ' ';} ?>
								
					"> <!-- closes the class -->
						<a href="<?php the_permalink() ?>" rel="bookmark">
							
								<?php 
									
										the_post_thumbnail('large');
										
									?>	
								<h3><?php the_title(); ?></h3>	
						</a>
							<p class="location-holder">
								Available at: <?php 
									$sep = '';
								foreach((get_the_category()) as $cat) {
									if (!($cat->cat_name=='packages')) echo $cat->cat_name; echo $sep = ', '; 
									} 
								?>	
							</p>
					</div>
					
				
				
			<?php endwhile; ?>
			<?php while(!$catquery->have_posts()) : $catquery->the_post(); ?>
				<p>We're sorry, there are no results that match this query.</p>
			<?php endwhile; ?>
            
		</div>	
			
		</main>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>