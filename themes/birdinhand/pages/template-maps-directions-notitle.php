<?php
/*
Template Name: Maps and Directions (No Title)
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
		<div class="banner inner-block" style="height: 0;">
			<div id="map_canvas"></div> <!-- Blank holder for Google Maps -->

			<article class="text-block inner-block">
				<?php if($uptitle=get_field("uptitle")):?>
					<strong class="title"><?php echo $uptitle;?></strong>
				<?php endif;?>
				
			</article>
		

		</div>
<script>
function goBack() {
    window.history.back();
}
</script>




					<div class="row">
                        <style>.fixed-block {background: transparent;}</style>
						<?php get_template_part( 'blocks/popups'); ?>
<?php 
if ( is_page(1416)) {
    
} else { 
?>
						<?php get_template_part( 'blocks/reservations'); ?>
						<?php } ?>
                <!-- reservations were here -->
			<div class="block-wrap">
		
							<div class="col-md-2">
				<div class="col-md-12 col-md-offset-1">
                	<?php $bih_switcher_logo = get_field('bih_feature_image_1'); ?>
					<img width="80%" src="<?php if($bih_switcher_logo): echo $bih_switcher_logo; else: ?>http://birdinhand.wpengine.com/wp-content/uploads/2015/12/main-logo1.png<?php endif; ?>" />
					
				</div>
			</div>
				
						
			<div class="text-wrap col-md-6">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>

					
					
					
						
						
										
				

				
					</div>
						<?php if( have_rows('property_information') ):?>
			<section class="property-box property-information container">
				<div class="row">
					<div class="col-md-12">
				
					
					<?php while( have_rows('property_information') ): the_row(); ?>
						<div class="col-md-3">
						<?php if( get_sub_field('farmstand') )
{
    ?> <h3 style="color:#7c2629"><?php the_sub_field("property_name") ?></h3> <?php
}
else
{
  ?> <h3><?php the_sub_field("property_name") ?></h3> <?php
} ?>
							
							<p><?php the_sub_field("property_address"); ?></p>
							
						</div>
					<?php endwhile; ?>	
		<?php endif; ?>	
		
					
							<?php if(get_field('contact_form')): ?>
							
								
						
							<div class="col-md-3">
                                <h1>Get In Touch With Us</h2>
									<?php $contact_shortcode = get_field('contact_form'); ?>
									<?php echo do_shortcode($contact_shortcode) ?>
							</div>		
							<?php endif; ?>
							
									<?php if(get_field('donation_request_button')): ?>
							<div class="col-md-3">
										<a class="button" href="<?php the_field("donation_request_button"); ?>">Donation Request</a>
										<br />
										<a class="button" href="<?php the_field("employment_application"); ?>">Employment Application</a>
							</div>		
							<?php endif; ?>
							
		
					</div>
				
				</div>
			</section>

	
	

		<div class="form-holder">
			<div class="container">
				<div class="block">
					<?php if($gravity_form=get_field("inner_gravity_form")):?>
						<div class="col">
							<?php echo do_shortcode($gravity_form);?>
						</div>
					<?php endif;?>
					<?php if( have_rows('cols') ):?>
						<?php while( have_rows('cols') ): the_row();?>
							<div class="col">
								<?php if($title=get_sub_field("title")):?>
									<h3><?php echo $title;?></h3>
								<?php endif;?>
								<?php if($link=esc_url(get_sub_field("link"))):?>
									<a href="<?php echo $link;?>" class="button"><?php _e("Learn More","birdinhand")?></a>
								<?php endif;?>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</main>
<?php endwhile; ?>
<?php get_footer(); ?>