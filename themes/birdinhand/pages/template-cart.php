<?php 
/*
Template Name: Cart Template
*/
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<main id="main" role="main">
<style type="text/css">
	.banner { display:none; }
</style>
	<div class="container" style="padding-top:250px;">
		
			<div id="content">
				<div class="text-wrap col-md-10">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					
					
					<?php edit_post_link( __( 'Edit', 'birdinhand' ) ); ?>
				<?php endwhile; ?>
				<?php wp_link_pages(); ?>
				<?php comments_template(); ?>
			</div>
        </div>
    <br />
    <?php if( get_field('timeline_embed') ): ?>
            <?php the_field("timeline_embed"); ?>
				<?php endif; ?>
		</main>

<?php get_footer(); ?>
<?php echo get_post_type(); ?>