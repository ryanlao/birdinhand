<?php
/*
Template Name: Main Property Page Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
		<div class="banner inner-block"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
		<article class="text-block inner-block">
			
					<strong class="title"><?php the_field("uptitle"); ?></strong>
				
				<h1><?php the_field("main_title"); ?><h1>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php if($certificate_logo=get_field("certificate_logo")):?>
				<div class="image-block">
					<?php the_field("certificate_logo");?>
				</div>
			<?php endif;?>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
	<?php get_template_part( 'blocks/reservations'); ?>
			<div class="block-wrap">
<?php get_template_part( 'blocks/property-switcher'); ?>
						
			<div class="text-wrap col-md-6">
				<?php the_content();?>
                
				<?php if( get_field('letter_button_link') ): ?>
                <div class="col-md-6">
					<a class="button" target="_blank" href="<?php the_field("letter_button_link"); ?>"><?php the_field("letter_button_text"); ?></a>
				</div>
                <?php endif; ?>
                    
                 <?php if( get_field('secondary_button') ): ?>
                <div class="col-md-6">
					<a class="button" href="<?php the_field("secondary_button"); ?>"><?php the_field("secondary_button_text"); ?></a>
				</div>
                <?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>
			<section class="rooms-box">
		
					<h1><?php the_field('room_images_title'); ?></h1>

				
				<div class="row lower-images">
<?php   $column_numbers = count( get_field('lower_images') );
        if ($column_numbers == 1):
        $bootstrap_columns = 12;
        elseif ($column_numbers == 2):
        $bootstrap_columns = 6;
        elseif ($column_numbers == 3):
        $bootstrap_columns = 4;
        elseif ($column_numbers == 4):
        $bootstrap_columns = 3;
        endif;
         ?>

    <?php if( have_rows('lower_images') ): ?>

    <?php while( have_rows('lower_images') ): the_row();?>
    <div class="col-sm-<?php echo $bootstrap_columns; ?> col-xs-6">
        <a href="<?php the_sub_field('lower_image_link'); ?>" >
           
	       <img src="<?php the_sub_field('lower_image_image');?>" />
            <h2><?php the_sub_field('lower_image_title'); ?></h2>
	       <div class="room-caption">
               <em><?php the_sub_field('lower_image_caption');?></em>
           </div>
        </a>
    </div>

    <?php endwhile; ?>

    <?php endif; ?>
</div>

					<div class="row">
						<div class="col-md-4 col-md-offset-4 top-30">
						<?php if( get_field('link_to_rooms_button') ): ?>
			
							<a href="<?php the_field("link_to_rooms_button"); ?>" class="button"><?php the_field("link_to_rooms_button_text"); ?></a>
							
						<?php endif; ?>	
						</div>
					</div>
				</div>

			</section>
		
<!-- hide form holder for now
		<div class="form-holder">
			<div class="container">
				<div class="block">
					<?php if($gravity_form=get_field("inner_gravity_form")):?>
						<div class="col">
							<?php echo do_shortcode($gravity_form);?>
						</div>
					<?php endif;?>
					<?php if( have_rows('cols') ):?>
						<?php while( have_rows('cols') ): the_row();?>
							<div class="col">
								<?php if($title=get_sub_field("title")):?>
									<h3><?php echo $title;?></h3>
								<?php endif;?>
								<?php if($link=esc_url(get_sub_field("link"))):?>
									<a href="<?php echo $link;?>" class="button"><?php _e("Learn More","birdinhand")?></a>
								<?php endif;?>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
		</div>
		-->
	</main>
<?php endwhile; ?>
<?php get_footer(); ?>