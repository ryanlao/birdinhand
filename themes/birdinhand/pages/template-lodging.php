<?php
/*
Template Name: Lodging Landing Page Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
		<div class="banner inner-block"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
			<article class="text-block inner-block">
				<?php if($uptitle=get_field("uptitle")):?>
					<h1><?php echo $uptitle;?></h1>
				<?php endif;?>
		
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php if($certificate_logo=get_field("certificate_logo")):?>
				<div class="image-block">
					<img src="<?php echo $certificate_logo["sizes"]["thumbnail_152x140"];?>" alt="<?php echo $certificate_logo["alt"];?>">
				</div>
			<?php endif;?>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
<?php get_template_part( 'blocks/reservations'); ?>
	
		<div class="block-wrap">
		
						<?php get_template_part( 'blocks/property-switcher'); ?>
				
						
			<div class="text-wrap col-md-6">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>
		<?php if( have_rows('blocks') ):?>
			<div class="container">
				<div class="col-wrap" style="margin-bottom:0px !important;">
					<?php while( have_rows('blocks') ): the_row();?>
						<div class="col">
						<a href="<?php the_sub_field("property_details_page"); ?>">
							<div class="col-area">
								<?php if($image=get_sub_field("image")):?>
									<img src="<?php echo $image["sizes"]["thumbnail_400x280"];?>" alt="<?php echo $image["alt"];?>">
								<?php endif;?>
								<div class="text-block">
									<?php if($title=get_sub_field("title")):?>
										<h3 class="property-title"><?php echo $title;?></h3>
									<?php endif;?>
									<?php the_sub_field("text");?>
									<p><?php the_sub_field("property_description"); ?></p>
								</div>
							</div>
						</a>	
						</div>
					<?php endwhile;?>
				</div>
			</div>
		<?php endif;?>
		<!-- hide form holder for now
		<div class="form-holder">
			<div class="container">
				<div class="block">
					<?php if($gravity_form=get_field("inner_gravity_form")):?>
						<div class="col">
							<?php echo do_shortcode($gravity_form);?>
						</div>
					<?php endif;?>
					<?php if( have_rows('cols') ):?>
						<?php while( have_rows('cols') ): the_row();?>
							<div class="col">
								<?php if($title=get_sub_field("title")):?>
									<h3><?php echo $title;?></h3>
								<?php endif;?>
								<?php if($link=esc_url(get_sub_field("link"))):?>
									<a href="<?php echo $link;?>" class="button"><?php _e("Learn More","birdinhand")?></a>
								<?php endif;?>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
		</div>
		-->
	</main>
<?php endwhile; ?>
<?php get_footer(); ?>