<?php
/*
Template Name: Test Template
*/
get_header(); ?>
	<div class="container">
		<main id="main" role="main">
			<div id="content">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'blocks/popups'); ?>
					<?php the_title( '<div class="title"><h1>', '</h1></div>' ); ?>
					<?php the_post_thumbnail( 'full' ); ?>
					<?php the_content(); ?>
					<?php edit_post_link( __( 'Edit', 'birdinhand' ) ); ?>
				<?php endwhile; ?>
				<?php wp_link_pages(); ?>
				<?php comments_template(); ?>
			</div>
		</main>
	</div>
<?php get_footer(); ?>
<?php echo get_post_type(); ?>