<?php
/*
Template Name: Dining Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
		<div class="banner inner-block"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
			<article class="text-block inner-block">
				<?php if($uptitle=get_field("uptitle")):?>
					<strong class="title"><?php echo $uptitle;?></strong>
				<?php endif;?>
				<h1><?php the_field("main_title"); ?><h1>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php if($certificate_logo=get_field("certificate_logo")):?>
				<div class="image-block">
					<img src="<?php echo $certificate_logo["sizes"]["thumbnail_152x140"];?>" alt="<?php echo $certificate_logo["alt"];?>">
				</div>
			<?php endif;?>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
<?php get_template_part( 'blocks/reservations'); ?>
			<div class="block-wrap">

						
			<div class="text-wrap col-md-6">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>
			<section class="dining-box">
		
					<h1><?php the_field('lower_images_title'); ?></h1>
				<div class="row">
			
				
				
					<div class="col-md-4">
						<a href=""<?php the_field('lower_image_left_link'); ?>" >
						
							<img src="<?php the_field('lower_image_left');?>" />
                            <h2><?php the_field('lower_image_left_title'); ?></h2>
							<div class="room-caption"><em><?php the_field('lower_image_left_caption');?></em></div>
						</a>
					</div>
					<div class="col-md-4">
						<a href=""<?php the_field('lower_image_center_link'); ?>" >
						
							<img src="<?php the_field('lower_image_center');?>" />
                            <h2><?php the_field('lower_image_center_title'); ?></h2>
							<div class="room-caption"><em><?php the_field('lower_image_center_caption');?></em></div>
						</a>	
					</div>
					<div class="col-md-4">
						<a href="<?php the_field('lower_image_right_link'); ?>" >
						
							<img src="<?php the_field('lower_image_right');?>" />
                            <h2><?php the_field('lower_image_right_title'); ?></h2>
							<div class="room-caption"><em><?php the_field('lower_image_right_caption');?></em></div>
						</a>	
					</div>
					
				</div>

			</section>


		<div class="form-holder">
			<div class="container">
				<div class="block">
					<?php if($gravity_form=get_field("inner_gravity_form")):?>
						<div class="col">
							<?php echo do_shortcode($gravity_form);?>
						</div>
					<?php endif;?>
					<?php if( have_rows('cols') ):?>
						<?php while( have_rows('cols') ): the_row();?>
							<div class="col">
								<?php if($title=get_sub_field("title")):?>
									<h3><?php echo $title;?></h3>
								<?php endif;?>
								<?php if($link=esc_url(get_sub_field("link"))):?>
									<a href="<?php echo $link;?>" class="button"><?php _e("Learn More","birdinhand")?></a>
								<?php endif;?>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</main>
<?php endwhile; ?>
<?php get_footer(); ?>