<?php
/*
Template Name: Contact Page Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
		<div class="banner inner-block"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
			<article class="text-block inner-block">
				<?php if($uptitle=get_field("uptitle")):?>
					<strong class="title"><?php echo $uptitle;?></strong>
				<?php endif;?>
				<h1><?php the_field("main_title"); ?><h1>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php if($certificate_logo=get_field("certificate_logo")):?>
				<div class="image-block">
					<img src="<?php echo $certificate_logo["sizes"]["thumbnail_152x140"];?>" alt="<?php echo $certificate_logo["alt"];?>">
				</div>
			<?php endif;?>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
<?php get_template_part( 'blocks/reservations'); ?>
		<div class="block-wrap">
		
						<?php get_template_part( 'blocks/property-switcher'); ?>
				
						
			<div class="text-wrap col-md-6">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>
		<?php if( have_rows('property_information') ):?>
			<section class="property-box">
				<div class="row">
					<div class="col-md-12">
				
					
					<?php while( have_rows('property_information') ): the_row(); ?>
						<div class="col-md-3">
							<h3><?php the_sub_field("property_name") ?></h3>
							<p><?php the_sub_field("property_address"); ?></p>
						</div>
					<?php endwhile; ?>	
		<?php endif; ?>	
                        
                        <?php if(get_field('contact_form')):?>
                            <div class="col-md-3">
                                <?php the_field('contact_form')?>
                              
                            </div>
                        <?php endif; ?>	
                        <?php if(get_field('donation_request_button')):?>
                            <div class="col-md-3">
                                <a class="button" href="<?php the_field("donation_request_button")?>" >Donation Request</a>
                                <br />
                                <a class="button" href="<?php the_field("employment_application")?>" >Employment Application</a>
                            </div>
                        <?php endif; ?>	
					</div>
				
				</div>
			</section>
	


	</main>
<?php endwhile; ?>
<?php get_footer(); ?>