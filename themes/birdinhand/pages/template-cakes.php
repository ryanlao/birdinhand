<?php 
 /*
Template Name: Cakes Template
*/
 get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<main id="main" role="main">
<div class="banner inner-block"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
			<article class="text-block inner-block">
				<?php if($uptitle=get_field("uptitle")):?>
					<strong class="title"><?php echo $uptitle;?></strong>
				<?php endif;?>
				<h1><?php the_field("main_title"); ?><h1>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
    <?php get_template_part( 'blocks/popups'); ?>
</div>
	<div class="container">
		
			<div id="content">
				<div class="text-wrap col-md-10">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php
                        if( have_rows('photo_gallery') ): while ( have_rows('photo_gallery') ) : the_row(); ?>
                               <div class="col-md-3" style="margin-top: 10px;">
                                        <?php 

                                            $image = get_sub_field('image');
                                            $size = 'thumbnail_260x265'; 

                                            if( $image ) {

	                                            echo wp_get_attachment_image( $image, $size );

                                            }

                                            ?>
                                </div>
                               

                           <?php endwhile; ?>

                       <?php endif; ?>

                        ?>
                        
                <?php 

                    $image = get_field('image');
                    $size = 'full'; // (thumbnail, medium, large, full or custom size)

                    if( $image ) {

	                    echo wp_get_attachment_image( $image, $size );

                    }

                    ?>
					
					<?php edit_post_link( __( 'Edit', 'birdinhand' ) ); ?>
				<?php endwhile; ?>
				<?php wp_link_pages(); ?>
				<?php comments_template(); ?>
			</div>
        </div>
    <br />
    <?php if( get_field('timeline_embed') ): ?>
            <?php the_field("timeline_embed"); ?>
				<?php endif; ?>
		</main>

<?php get_footer(); ?>
<?php echo get_post_type(); ?>