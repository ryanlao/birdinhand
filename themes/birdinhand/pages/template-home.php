<?php
/*
Template Name: Home Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
		<div class="banner"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
			<article class="text-block ">
				<?php the_title('<h1>','</h1>');?>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
		<?php get_template_part( 'blocks/reservations'); ?>
		<?php if( have_rows('cols') ):?>
			<div class="three-col">
				<?php while( have_rows('cols') ): the_row();?>
					<div class="col">
						<?php if($image=get_sub_field("image")):?>
							<img src="<?php echo $image["sizes"]["thumbnail_548x351"];?>" alt="<?php echo $image["alt"];?>">
						<?php endif;?>
						<?php if($link=esc_url(get_sub_field("link"))):?>
							<?php
								if(get_sub_field("color")=="yellow")$class=" yellow";
								elseif(get_sub_field("color")=="green")$class=" green";
								elseif(get_sub_field("color")=="red")$class=" red";
								else $class="";
							?>
							<a href="<?php echo $link;?>" class="text-box<?php echo $class?>">
						<?php else:?>
							<div class="text-box">
						<?php endif;?>
							<?php if($title=get_sub_field("title")):?>
								<strong class="title"><?php echo $title;?></strong>
							<?php endif;?>
							<?php if($sub_title=get_sub_field("sub_title")):?>
								<span class="category-title"><?php echo $sub_title;?></span>
							<?php endif;?>
						<?php if($link):?>
							</a>
						<?php else:?>
							</div>
						<?php endif;?>
					</div>
				<?php endwhile;?>
			</div>
		<?php endif;?>
		<div class="block-area">
					<?php echo do_shortcode( '[gravityform id="14" title="false"]' ); ?>
					<?php echo do_shortcode( '[gravityform id="15" title="false"]' ); ?>
			<div class="centered-icons">
									<?php if(have_rows('social_dropdowns') ): ?>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-facebook"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu">
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayAmishCountry">Amish Country
                Motel</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/EatBIHBakery">Bird-in-Hand Bakery
                &amp; Cafe</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayBIHFamilyInn/">Bird-in-Hand
                Family Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/EnjoyBIHRestaurantStage">Bird-in-Hand
                Family Restaurant &amp; Smorgasbord</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/BirdInHandStage">Bird-in-Hand
                Stage</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayBIHVillageInn">Bird-in-Hand
                Village Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayCountryAcres">Country Acres
                Campground</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayTravelersRest">Travelers Rest
                Motel</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-twitter"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu" style=
        "display: none;">
            <li>
                <a class="property-social-link" href=
                "https://twitter.com/BirdinHandNews">Bird-in-Hand</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-google-plus"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu">
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/108711304751365443917/posts">Amish
                Country Motel</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/108612624412987899327/posts">Bird-in-Hand
                Bakery &amp; Cafe</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/116608081945696114872/posts">Bird-in-Hand
                Family Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/100596160016055074734/posts">Bird-in-Hand
                Family Restaurant &amp; Smorgasbord</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/109285815040234906036/posts">Bird-in-Hand
                Stage</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/114076772192103625031/posts">Bird-in-Hand
                Village Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/109935983771991635595/posts">Country
                Acres Campground</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/103298858345041402585/posts">Travelers
                Rest Motel</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-youtube"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu" style=
        "display: none;">
            <li>
                <a class="property-social-link" href=
                "https://www.youtube.com/channel/UC02Inlavo6OYDW2nEn53qtQ">Bird-in-Hand</a>
            </li>
        </ul>
    </div>
	<?php endif; ?>
	</div>
			
		</div>
		<div class="block-holder">
			<div class="col-holder">
				<?php if($welcome_text=get_field("welcome_text_lower")):?>
					<h1 style="text-align:center;"><?php echo $welcome_text;?></h1>
				<?php endif;?>
				<?php if($logo=get_field("logo")):?>
					<img src="<?php echo $logo["sizes"]["thumbnail_367x335"];?>" alt="<?php echo $logo["alt"];?>">
				<?php endif;?>
			</div>
			<div class="text-holder">
				<?php if($block_title=get_field("block_title")):?>
					<h2><?php echo $block_title;?></h2>
				<?php endif;?>
				<?php the_field("block_text")?>
			</div>
			<div class="yellow-block">
				<?php if($block_image=get_field("block_image")):?>
					<div class="image-area">
						<img src="<?php echo $block_image["sizes"]["thumbnail_557x363"];?>" alt="<?php echo $block_image["alt"];?>">
					</div>
				<?php endif;?>
				<?php if($block_link=esc_url(get_field("block_link"))):?>
					<a href="<?php echo $block_link;?>" class="link"><?php _e("read more","birdinhand")?><i class="icon-arrow"></i></a>
				<?php endif;?>
			</div>
		</div>
		<?php if( have_rows('sections') ):?>
			<?php while( have_rows('sections') ): the_row();?>
				<div class="row">
					<div class="col-md-12 box-wrap box">
						<div class="col-md-7 col-sm-12 home-img <?php if(!get_sub_field("picture_left")):?> pull-right<?php endif;?>"
							<?php if($image=get_sub_field("image")):?>
								style="background-image: url('<?php echo $image;?>');"
							<?php endif;?>
						>
							<div class="text-holder col-sm-12">
								<?php if($image_title=get_sub_field("image_title")):?>
									<strong class="title"><?php echo $image_title;?></strong>
								<?php endif;?>
								
							</div>
						</div>
						<div class="col-md-5 col-sm-12 <?php if(get_sub_field("picture_left")):?> first-row<?php endif;?>">
							<div class="homepage-col-text col-sm-12">
								<?php if($title=get_sub_field("title")):?>
									<h2><?php echo $title;?></h2>
								<?php endif;?>
								<?php the_sub_field("text");?>
								<?php if($button_text=get_sub_field("button_text")):?>
										<a href="<?php the_sub_field("link")?>" class="button"><?php echo $button_text?></a>
									<?php endif;?>
							</div>
						</div>
					</div>
				</div>	
			<?php endwhile;?>
		<?php endif;?>
		<?php
			if($related=get_field("post_on_home")):
			$args = array (
			'ignore_sticky_posts'	=> 1,
			'post_type'				=> 'post',
			'posts_per_page'		=> '-1',
			'post__in'				=> $related,
			'orderby'				=> 'post__in',
			);
			$the_query = new WP_Query( $args );
		?>
		<?php if ($the_query->have_posts()) : ?>
			<section class="carousel-box">
				<?php if($carousel_title=get_field("carousel_title")):?>
					<h1><?php echo $carousel_title;?></h1>
				<?php endif;?>
				<div class="carousel">
					<div class="mask">
						<div class="slideset">
							<?php $i=0;while ($the_query->have_posts()) : $the_query->the_post();$i++ ?>
								<?php if($i%4==1):?><div class="slide"><?php endif;?>
									<div class="slide-holder">
										<a href="<?php the_permalink()?>" class="bg-cover">
											<?php the_post_thumbnail("thumbnail_260x265");?>
											<div class="text-area">
												<strong class="sub-title"><em><?php _e("View","birdinhand")?></em> <?php _e("Details","birdinhand")?><i class="icon-arrow"></i></strong>
											</div>
										</a>
										<?php the_title('<strong class="title">','</strong>');?>
									</div>
								<?php if($i%4==0):?></div><?php endif;?>
							<?php endwhile;?>
							<?php if($i%4!=0):?></div><?php endif;?>
						</div>
					</div>
					<a class="btn-prev" href="#"><i class="icon-arrow-left"></i></a>
					<a class="btn-next" href="#"><i class="icon-arrow-right"></i></a>
				</div>
			</section>
		<?php endif;?>
		<?php wp_reset_postdata();?>
		<?php endif;?>
	</main>
<?php endwhile; ?>
<?php get_footer(); ?>