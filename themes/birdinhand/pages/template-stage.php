<?php
/*
Template Name: Stage Main Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" class="stage" role="main">
	
		<div class="banner inner-block">
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
			<article class="text-block">
				<?php the_title('<h1>','</h1>');?>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>
<?php get_template_part('blocks/reservations'); ?>
	<?php if( have_rows('cols') ):?>	
		<div class="row">
			<div class="four-col">
				<?php while( have_rows('cols') ): the_row();?>
					<div class="col col-md-3">
						<?php if($image=get_sub_field("image")):?>
							<img src="<?php echo $image["sizes"]["thumbnail_548x351"];?>" alt="<?php echo $image["alt"];?>">
						<?php endif;?>
						<?php if($link=esc_url(get_sub_field("link"))):?>
							<?php
								if(get_sub_field("color")=="yellow")$class=" green";
								elseif(get_sub_field("color")=="dark")$class=" brown";
								else $class="";
							?>
							<a href="<?php echo $link;?>" class="text-box<?php echo $class?>">
						<?php else:?>
							<div class="text-box">
						<?php endif;?>
							<?php if($title=get_sub_field("title")):?>
								<strong class="title"><?php echo $title;?></strong>
							<?php endif;?>
							<?php if($sub_title=get_sub_field("sub_title")):?>
								<span class="category-title"><?php echo $sub_title;?></span>
							<?php endif;?>
						<?php if($link):?>
							</a>
						<?php else:?>
							</div>
						<?php endif;?>
					</div>
				<?php endwhile; ?>
			</div>
		</div>	
	<?php endif;?>
	

		<div class="row">
			<div class="col-md-12">
				<h2><?php the_title();?></h2>
				<div class="row">
				<?php the_content();?>
				</div>

			</div>
			<?php if(have_rows('sections')) : ?>
				<?php while( have_rows('sections') ): the_row();?>
				<div class="row">
					<div class="col-md-12 box-wrap box">
						<div class="col-md-7 property-img <?php if(!get_sub_field("picture_left")):?> pull-right<?php endif;?>"
							<?php if($image=get_sub_field("image")):?>
								style="background-image: url('<?php echo $image;?>');"
							<?php endif;?>
					
							<div class="text-holder">
								<?php if($image_title=get_sub_field("image_title")):?>
									<strong class="title"><?php echo $image_title;?></strong>
								<?php endif;?>
								
							</div>
						</div>
						<div class="col-md-5 <?php if(get_sub_field("picture_left")):?> first-row<?php endif;?>">
							<div class="homepage-col-text">
								<?php if($title=get_sub_field("title")):?>
									<h2><?php echo $title;?></h2>
								<?php endif;?>
								<?php the_sub_field("text");?>
								<?php if($button_text=get_sub_field("button_text")):?>
										<a href="<?php the_sub_field("link")?>" class="button"><?php echo $button_text?></a>
									<?php endif;?>
							</div>
						</div>
					</div>
				</div>	
			<?php endwhile;?>
		<?php endif;?>

					  
					  
		
							
						 

		</div>

				
				
				
	</div>

	</main>
<?php endwhile; ?>
<?php get_footer(); ?>