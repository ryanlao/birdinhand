<?php
/*
Template Name: Group Sales & Menu Main Template
*/
get_header(); ?>
<?php while ( have_posts( ) ) : the_post(); ?>
	<main id="main" role="main">
	
		<div class="banner"
			<?php if(has_post_thumbnail()):?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail_1680x896' );?>
				style="background-image: url(<?php echo $image[0];?>);"
			<?php endif;?>
		>
			<article class="text-block inner-block">
				<?php if($uptitle=get_field("uptitle")):?>
					<strong class="title"><?php echo $uptitle;?></strong>
				<?php endif;?>
				<h1><?php the_field("main_title"); ?><h1>
				<?php if($header_btn_link=esc_url(get_field("header_btn_link"))):?>
					<a href="<?php echo $header_btn_link;?>" class="button"><?php the_field('header_btn_text');?> <i class="icon-arrow"></i></a>
				<?php endif;?>
			</article>
			<?php get_template_part( 'blocks/popups'); ?>
		</div>

		<?php if( have_rows('cols') ):?>
			<div class="three-col">
				<?php while( have_rows('cols') ): the_row();?>
					<div class="col">
						<?php if($image=get_sub_field("image")):?>
							<img src="<?php echo $image["sizes"]["thumbnail_548x351"];?>" alt="<?php echo $image["alt"];?>">
						<?php endif;?>
						<?php if($link=esc_url(get_sub_field("link"))):?>
							<?php
								if(get_sub_field("color")=="yellow")$class=" green";
								elseif(get_sub_field("color")=="dark")$class=" brown";
								else $class="";
							?>
							<a href="<?php echo $link;?>" class="text-box<?php echo $class?>">
						<?php else:?>
							<div class="text-box">
						<?php endif;?>
							<?php if($title=get_sub_field("title")):?>
								<strong class="title"><?php echo $title;?></strong>
							<?php endif;?>
							<?php if($sub_title=get_sub_field("sub_title")):?>
								<span class="category-title"><?php echo $sub_title;?></span>
							<?php endif;?>
						<?php if($link):?>
							</a>
						<?php else:?>
							</div>
						<?php endif;?>
					</div>
				<?php endwhile;?>
			</div>
		<?php endif;?>

			<div class="block-wrap">
		
					
				
						
			<div class="text-wrap col-md-10 col-md-offset-1 col-sm-12 ">
				<?php the_content();?>
				<?php if( get_field('letter_button_link') ): ?>
					<a class="button" href="<?php the_field("letter_button_link"); ?>"><?php the_field(letter_button_text); ?></a>
				<?php endif; ?>
			</div>
					<?php get_template_part( 'blocks/testimonials-videos'); ?>
		</div>
			<div class="block-wrap text-wrap col-md-10 col-md-offset-1 col-sm-12  jpibfi_container">

					  
					  <div class="col-md-8">

	<?php if( have_rows('tabs') ):?>				  
	<div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
        <ul class="nav nav-tabs" id="myTabs" role="tablist">
        <?php while( have_rows('tabs') ): the_row(); 
			$tab_title_string = get_sub_field("tab_title");
			$tab_id = preg_replace("/[^a-zA-Z0-9]/", "", $tab_title_string);
        ?>
            <li class="" role="presentation">
                <a aria-controls="<?php echo $tab_id; ?>" aria-expanded="true" data-toggle="tab"
                href="<?php echo $tab_id; ?>" id="<?php echo $tab_id; ?>-tab" role="tab"><?php the_sub_field("tab_title"); ?></a>
            </li>
        <?php endwhile; ?>
        </ul>
        <div class="tab-content" id="myTabContent">
        <?php while( have_rows('tabs') ): the_row(); 
			$tab_title_string = get_sub_field("tab_title");
			$tab_id = preg_replace("/[^a-zA-Z0-9]/", "", $tab_title_string);
        ?>
            <div aria-labelledby="<?php echo $tab_id; ?>-tab" class="tab-pane well fade in <?php echo $tab_id; ?>" id="<?php echo $tab_id; ?>" role="tabpanel">
            <?php the_sub_field("tab_content"); ?>
											<?php if (get_field("menu_button_text")): ?>
												<a class="button gform_button" href="<?php the_sub_field("menu_button"); ?>"><?php the_sub_field("menu_button_text"); ?></a>
			<?php endif; ?>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>
<script type="text/javascript">
	jQuery('.tab-content > div:first-of-type').addClass("active");
	jQuery('.tab-content > div:first-of-type').addClass("in");
	jQuery('.nav-tabs li:first-of-type').addClass("active");
	jQuery('#myTabs a').click(function (e) {
	  e.preventDefault();
	  var TabLink = jQuery(this).attr('href');
	  jQuery('.tab-pane').removeClass("active");
	  jQuery('.tab-pane.' + TabLink).addClass("active");
	})
</script>
						
						</div>
						<div class="col-md-4">
								<h3>Contact Our Sales Team</h3>
								<?php echo do_shortcode(get_field("group_gravity_form"));?>

						</div>

					  
					  
		
							
						 

		</div>

				
				
				
	</div>

	</main>
<?php endwhile; ?>
<?php get_footer(); ?>