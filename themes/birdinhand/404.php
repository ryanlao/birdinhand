<?php get_header(); ?>
	<div class="container">
		<main id="main" role="main">
			<div id="content">
				<?php get_template_part( 'blocks/not_found' ); ?>
			</div>
		</main>
	</div>
<?php get_footer(); ?>