var w_monthname = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
var w_Adults = 1;
var w_Children = 0;

var URL = 'PropertyURL';
var AliasID = '0000000000';
var ServerSite = 'east';

//Eres Premium ID
var id = 'EnterPropertyIDhere';

function BookNow() {
    if (id == '')
    {
        jQuery("#w_lblErrorMessage").show();
        return false;
    }
    var ArrivalDate = jQuery("#w_hfArrivalDate").val();
    var Nights = jQuery("#w_txtNights").val();
    var PromoCode = jQuery("#w_txtPromoCode").val();

    if ((AliasID != "0000000000") || (id != 'EnterPropertyIDhere')) {
        var BookingSite = 'https://www.bookonthenet.net/' + ServerSite
                                                          + '/premium/eresmain.aspx?id=' + id
                                                          + '&arrival_date=' + ArrivalDate
                                                          + '&stay_nights=' + Nights
                                                          + '&adults=' + w_Adults
                                                          + '&children=' + w_Children;
        if (PromoCode != '') {
            BookingSite = BookingSite + '&promo_code=' + PromoCode;
        }

        window.open(BookingSite, 'EresBook');
    }
    else {
        var ErrorMsg = 'Please Select a Property.';
        jQuery("#w_lblErrorMessage").show();
        jQuery("#w_lblErrorMessage").html(ErrorMsg);
    }
};

jQuery(function () {
    // Datepicker
    jQuery("#w_txtArrivalDate").datepicker({
        showOn: "button",
        buttonImage: "http://bird-in-hand.com/wp-content/plugins/reservation-widget/inc/vendor/roomkey/widget/general/ew_images/Calendar.png",
        buttonImageOnly: true,
        dateFormat: 'M d, yy',
        minDate: new Date(),
        buttonText: 'Select Arrival Date',
        changeMonth: true,
        changeYear: true
    });

    jQuery("#w_txtDepartureDate").datepicker({
        showOn: "button",
        buttonImage: "http://bird-in-hand.com/wp-content/plugins/reservation-widget/inc/vendor/roomkey/widget/general/ew_images/Calendar.png",
        buttonImageOnly: true,
        dateFormat: 'M d, yy',
        minDate: '+1d',
        maxDate: '+180d',
        buttonText: 'Select Departure Date',
        changeMonth: true,
        changeYear: true
    });

    ResetDates();
});

function SetArrivalDate() {
    var c1ArrivalDate = new Date(jQuery("#w_txtArrivalDate").val());
    var ONE_DAY = 1000 * 60 * 60 * 24;
    var c1ArrivalDate_ms = c1ArrivalDate.getTime();
    var c1MinDate_ms = c1ArrivalDate_ms + ONE_DAY;
    var c1MinDate = new Date(c1MinDate_ms);
    var c1MaxDate_ms = c1ArrivalDate_ms + (ONE_DAY * 180);
    var c1MaxDate = new Date(c1MaxDate_ms);
    var c1ArrivalYear = c1ArrivalDate.getFullYear();
    var c1ArrivalMonth = c1ArrivalDate.getMonth() + 1;
    var c1ArrivalDay = c1ArrivalDate.getDate();
    jQuery("#w_hfArrivalDate").val(c1ArrivalYear + '-' + c1ArrivalMonth + '-' + c1ArrivalDay);
    jQuery("#w_txtDepartureDate").datepicker("option", "minDate", c1MinDate);
    jQuery("#w_txtDepartureDate").datepicker("option", "maxDate", c1MaxDate);
    NightsChanged();
};

function SetDepartureDate() {
    jQuery("#w_lblErrorMessage").hide();
    var ONE_DAY = 1000 * 60 * 60 * 24;
    var c1ArrivalDate = new Date(jQuery("#w_txtArrivalDate").val());
    var c1DepartureDate = new Date(jQuery("#w_txtDepartureDate").val());

    var c1days = Math.round((c1DepartureDate.getTime() - c1ArrivalDate.getTime()) / ONE_DAY);
    jQuery("#w_txtNights").val(c1days);
};

function ResetDates() {
    var c1ArrivalDate = new Date();
    var c1ArrivalYear = c1ArrivalDate.getFullYear();
    var c1ArrivalMonth = c1ArrivalDate.getMonth() + 1;
    var c1ArrivalDay = c1ArrivalDate.getDate();

    jQuery("#w_lblErrorMessage").hide();
    jQuery("#w_txtArrivalDate").val(w_monthname[c1ArrivalDate.getMonth()] + ' ' + c1ArrivalDay + ', ' + c1ArrivalYear);
    jQuery("#w_hfArrivalDate").val(c1ArrivalYear + '-' + c1ArrivalMonth + '-' + c1ArrivalDay);
    jQuery("#w_txtNights").val("1");

    var ONE_DAY = 1000 * 60 * 60 * 24;
    var c1ArrivalDate_ms = c1ArrivalDate.getTime();
    var c1MinDate_ms = c1ArrivalDate_ms + ONE_DAY;
    var c1MinDate = new Date(c1MinDate_ms);
    var c1MaxDate_ms = c1ArrivalDate_ms + (ONE_DAY * 180);
    var c1MaxDate = new Date(c1MaxDate_ms);
    jQuery("#w_txtDepartureDate").datepicker("option", "minDate", c1MinDate);
    jQuery("#w_txtDepartureDate").datepicker("option", "maxDate", c1MaxDate);
    NightsChanged();
};

function CheckNumbers(e) {
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) {
        keynum = e.keyCode;
    }
    else if (e.which) {
        keynum = e.which;
    }

    keychar = String.fromCharCode(keynum);
    numcheck = /\d/;
    return numcheck.test(keychar);
};

function NightsChanged() {
    if (jQuery("#w_txtNights").val() <= 0) {
        jQuery("#w_lblErrorMessage").hide();

        if (jQuery("#w_txtNights").val() == 0) {
            jQuery("#w_txtNights").val("1");
        }
        else {
            jQuery("#w_txtNights").val(Math.abs(jQuery("#w_txtNights").val()));
        }
    }
    else if (jQuery("#w_txtNights").val() > 180) {
        jQuery("#w_txtNights").val("1");
        var ErrorMsg = 'Max reservation length is 180 days.';
        jQuery("#w_lblErrorMessage").show();
        jQuery("#w_lblErrorMessage").html(ErrorMsg);
    }
    else {
        jQuery("#w_lblErrorMessage").hide();
    }

    var c1ArrivalDate = new Date(jQuery("#w_txtArrivalDate").val());
    var ONE_DAY = 1000 * 60 * 60 * 24;
    // Convert date to milliseconds
    var c1ArrivalDate_ms = c1ArrivalDate.getTime();
    var c1DepartureDate_ms = c1ArrivalDate_ms + (ONE_DAY * parseInt(jQuery("#w_txtNights").val()));
    var c1DepartureDate = new Date(c1DepartureDate_ms);

    var c1DepartureYear = c1DepartureDate.getFullYear();
    var c1DepartureMonth = c1DepartureDate.getMonth() + 1;
    var c1DepartureDay = c1DepartureDate.getDate();
    jQuery("#w_txtDepartureDate").val(w_monthname[c1DepartureDate.getMonth()] + ' ' + c1DepartureDay + ', ' + c1DepartureYear);
};

function ScrollDateDown() {
    var w_nights = parseInt(jQuery("#w_txtNights").val());
    if (w_nights > 1) {
        jQuery("#w_txtNights").val(w_nights - 1);
        NightsChanged();
    }
};

function ScrollDateUp() {
    var w_nights = parseInt(jQuery("#w_txtNights").val());
    if (w_nights < 180) {
        jQuery("#w_txtNights").val(w_nights + 1);
        NightsChanged();
    }
};

function SetAdults() {
    w_Adults = jQuery("#ddAdults").val();
};

function SetChildren() {
    w_Children = jQuery("#ddChildren").val();
};

function SetProperty() {
    id = jQuery("#ddProperties").val();
    if (id != '')
        jQuery("#w_lblErrorMessage").hide();

};

