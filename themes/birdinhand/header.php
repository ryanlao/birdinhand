  <!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
		<script type="text/javascript">
		  window.onresize = displayWindowSize;
		  window.onload = displayWindowSize;
		  function displayWindowSize() {
			  var myWidth = 0;
			  if( typeof( window.innerWidth ) == 'number' ) {
				  myWidth = window.innerWidth; 
			  } else if( document.documentElement && ( document.documentElement.clientWidth ) ) {
				  myWidth = document.documentElement.clientWidth;
			  } else if( document.body && ( document.body.clientWidth ) ) {
				  myWidth = document.body.clientWidth; 
			  }
			  //document.getElementById("dimensions").innerHTML = myWidth;
		  };		
		</script>
	<link rel="stylesheet" type="text/css" href="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/maps/map.css" />

        <link type="text/css" href="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/res-assets/ew_css/black-tie/jquery-ui-1.8.9.custom.css" rel="stylesheet" />

     
        <script type="text/javascript" src="http://birdinhand.wpengine.com/wp-content/themes/birdinhand/res-assets/ew_js/CalendarWidget.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css">
     <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
  <![endif]-->
</head>
<?php if(is_page_template("pages/template-home.php") or is_page_template("pages/template-inner.php"))$class="inner";else $class="";?>
<body <?php body_class($class); ?>>
<span id="dimensions"></span> 
	<div id="wrapper">
		<span class="overlay"></span>
		<header id="header">
			<div class="header-top">
				<div class="contained">
<script type="text/javascript">
    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

    if(isSafari) { 
        jQuery('#header-social').css("margin-top", "-30px");
    }
</script>
					<div class="block">
					  <?php 
                        if(get_field("welcome_text"))$welcome_text=get_field("welcome_text");
                        elseif(get_field("welcome_text","options"))$welcome_text=get_field("welcome_text","options");
                       ?>
						<div class="welcome"><span><?php echo $welcome_text;?>&nbsp;&nbsp;&nbsp;</span>
						<div id="header-social" style="float:right; margin-top:-5px;">
							
<?php if( have_rows('social_dropdowns') ): ?>
    <?php while( have_rows('social_dropdowns') ): the_row();?>
         <?php   $image = get_sub_field('image'); ?>
            <div class="social-net dropdown">
              <a class="fa <?php echo $image; ?>" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a><!-- self-close-->
             <?php if( have_rows('social_links') ): ?>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
            		<?php while( have_rows('social_links') ): the_row();?>
             			<?php $link = get_sub_field('link'); ?>
                		<?php $property = get_sub_field('property'); ?>
                    	<li>
                    		<a class="property-social-link" target="_blank" href="<?php echo $link; ?>">
                    			<?php echo $property; ?>
                			</a>
                		</li>
                    <?php endwhile;?>
                </ul>
        	<?php endif; ?>
        </div>
    <?php endwhile;?>
<?php endif; ?>
<?php if(!have_rows('social_dropdowns') ): ?>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-facebook"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu">
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayAmishCountry">Amish Country
                Motel</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/EatBIHBakery">Bird-in-Hand Bakery
                &amp; Cafe</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayBIHFamilyInn/">Bird-in-Hand
                Family Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/EnjoyBIHRestaurantStage">Bird-in-Hand
                Family Restaurant &amp; Smorgasbord</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/BirdInHandStage">Bird-in-Hand
                Stage</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayBIHVillageInn">Bird-in-Hand
                Village Inn & Suites</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayCountryAcres">Country Acres
                Campground</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://www.facebook.com/StayTravelersRest">Travelers Rest
                Motel</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-twitter"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu" style=
        "display: none;">
            <li>
                <a class="property-social-link" href=
                "https://twitter.com/BirdinHandNews">Bird-in-Hand</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-google-plus"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu">
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/108711304751365443917/posts">Amish
                Country Motel</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/108612624412987899327/posts">Bird-in-Hand
                Bakery &amp; Cafe</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/116608081945696114872/posts">Bird-in-Hand
                Family Inn</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/100596160016055074734/posts">Bird-in-Hand
                Family Restaurant &amp; Smorgasbord</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/109285815040234906036/posts">Bird-in-Hand
                Stage</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/114076772192103625031/posts">Bird-in-Hand
                Village Inn & Suites</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/109935983771991635595/posts">Country
                Acres Campground</a>
            </li>
            <li>
                <a class="property-social-link" href=
                "https://plus.google.com/103298858345041402585/posts">Travelers
                Rest Motel</a>
            </li>
        </ul>
    </div>
    <div class="social-net dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="fa fa-youtube"
        data-toggle="dropdown" id="dLabel" type="button"></a>
        <ul aria-labelledby="dLabel" class="dropdown-menu" style=
        "display: none;">
            <li>
                <a class="property-social-link" href=
                "https://www.youtube.com/channel/UC02Inlavo6OYDW2nEn53qtQ">Bird-in-Hand</a>
            </li>
        </ul>
    </div>
						<?php endif; ?>
						</div>
						<!-- <a href="#"><i class="fa fa-facebook-square"></i></a>
						<a href="https://twitter.com/BirdinHandNews"><i class="fa fa-twitter-square"></i></a>
						<a href="#"><i class="fa fa-google-plus-square"></i></a>
						<a href="#"><i class="fa fa-youtube-square"></i></a> -->
						<!-- <a href="#footer-link">&nbsp;Connect With Us <i class="fa fa-comments"></i></a> --></div>
					     
					</div>
					<?php
					  if(get_field("tel"))$tel=get_field("tel");
					  elseif(get_field("address_link","options"))$tel=get_field("tel","options");
					  if(get_field("address_link"))$address_link=esc_url(get_field("address_link"));
					  elseif(get_field("address_link","options"))$adsress_link=esc_url(get_field("address_link","options"));
					  if(get_field("email"))$email=antispambot(get_field("email"));
					  elseif(get_field("email","options"))$email=antispambot(get_field("email","options"));
					?>
					<?php if($tel or $address_link or $email):?>
					  <ul class="contact-list">
						  <?php if($tel):?>
							<li class="tel"><a href="tel:<?php echo $tel; ?>"><i class="icon-phone"></i><?php echo $tel; ?></a></li>
						  <?php endif;?>

						  <?php if($email):?>
							<li class="top-mail" ><a href="mailto:<?php echo $email?>"><i class="icon-mail"></i><?php echo $email?></a></li>
						  <?php endif;?>
						  
					  </ul>
					<?php endif;?>
					<div class="fa fa-map-marker hnl-button" aria-hidden="true"><a href="#">How To Find Us</a></div>
				</div>
			</div>
			<div class="logo">
				<a href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri()?>/images/logo.png" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
				</a>
			</div>
			<?php
			  if(get_field("email_mobile"))$email_mobile=antispambot(get_field("email_mobile"));
			  elseif(get_field("email_mobile","options"))$email_mobile=antispambot(get_field("email_mobile","options"));
			?>
			<?php if( has_nav_menu( 'primary_left' ) or has_nav_menu( 'primary_right' ) or $email_mobile):?>
			  <div class="heading">
				  <div class="container-fluid">
				  <?php if($email_mobile):?>
					<span class="mail">
					  <a href="mailto:<?php echo $email_mobile?>"><i class="icon-mail"></i><?php echo $email_mobile?></a>
					</span>
				  <?php endif;?>
				  <a href="#" class="opener"><span><?php _e("Menu","birdinhand")?></span></a>
					  <nav id="nav">
						  <?php
							if( has_nav_menu( 'primary_left' ))
							wp_nav_menu( array(
								'container' => false,
								'theme_location' => 'primary_left',
								'menu_class'     => 'nav-list',
								'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
								)
							);
						  ?>
						  <?php
							if( has_nav_menu( 'primary_right' ))
							wp_nav_menu( array(
								'container' => false,
								'theme_location' => 'primary_right',
								'menu_class'     => 'nav-list right',
								'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
								)
							);
						  ?>
					  </nav>
				  </div>
			  </div>
			<?php endif;?>
		</header>